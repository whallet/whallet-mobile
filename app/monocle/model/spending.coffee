class __Model.Spending extends Monocle.Model

    @fields "description", "amount", "amountDouble", "percentage", "percentageDouble", "position", "category"