class __Model.Budget extends Monocle.Model

	 @fields "id", "description", "description_tags", "created_at", "limit", "limitString", "tag_list", "categories", "spending", "left", "leftText", "progess", "color", "movements", "spendingFloat"