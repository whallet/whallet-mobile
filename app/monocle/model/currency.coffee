class __Model.Currency extends Monocle.Model

    @fields "id", "decimal_mark", "iso_code", "iso_numeric", "name", "symbol", "symbol_first", "thousands_separator", "example", "active"