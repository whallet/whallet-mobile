class __Model.Movement extends Monocle.Model

    @fields "id", "title", "description", "done", "created_at", "category", "categoryID", "date", "tags", "amount", "type", "formatDate", "amountDouble", "rule", "rule_locale", "endson", "endson_input", "has_recurrence"

    @kind_of: ->
        if movement.category == 10 or movement.category == "10" then "income" else "expense"