class NewBudgetCtrl extends Monocle.Controller

    events:
        "load section#newBudget"                        : "onLoadNewBudget"
        "tap section#newBudget > article div.amount"    : "tapNewAmount"
        "tap section#newBudget > article div.category"  : "onChangeCategory"
        "tap a[data-action=cancel]"                     : "onCancel"
        "tap a[data-action=save]"                       : "onSave"
        "tap a.button-tag"                              : "tapTag"

    elements:
        "#title_budget"                                 : "title_budget"
        "#amount"                                       : "amount"
        "#budget_txt-tag"                               : "budget_tags"
        "#carouselBudget"                               : "carousel"
        "#budget_cancel"                                : "budget_cancel"
        "#budget_accept"                                : "budget_accept"

    tapTag: (event) ->
        console.log "tapTag"
    
    onCancel: (event) ->
        console.log "onCancel"
        Lungo.Router.back()

    onSave: (event) ->
        console.log "onSave"

        budgetID = Lungo.Data.Storage.persistent("budgetID")

        console.log budgetID


        currencySymbol = Helpers.userCurrencySymbol()
        user = Lungo.Data.Storage.persistent("userInfo")

        tags = @budget_tags.val()
        if tags == ""
            tags = "no tag"

        categoryClass = Lungo.dom("img#budget_category")[0].className.split(" ")
        categoryId = getCategoryId(categoryClass[2])

        cleanAmount = @amount[0].innerText
        while (cleanAmount.toString().indexOf(user.currency.thousands_separator) != -1)
            cleanAmount = cleanAmount.toString().replace(user.currency.thousands_separator,"");
        cleanAmount = cleanAmount.replace(user.currency.decimal_mark,".")
        cleanAmount = cleanAmount.replace(currencySymbol,"")

        if cleanAmount > 0
            url = "http://www.whallet.com/api/v1/budget/new.json"
            tags = tags.replace /^\s+|\s+$/g, "" # trim("")
            tags = tags.replace /, /, ","
            post_data =
                token: Lungo.Data.Storage.persistent("tokenAuth"),
                amount: cleanAmount,
                category: categoryId,
                tags: tags

            budgetID = Lungo.Data.Storage.persistent("budgetID")
            if budgetID
                url = "http://www.whallet.com/api/v1/budget/update.json"
                post_data =
                    token: Lungo.Data.Storage.persistent("tokenAuth"),
                    amount: cleanAmount,
                    category: categoryId,
                    tags: tags,
                    id: budgetID

            Lungo.Service.Settings.error = errorResponse
            result = Lungo.Service.post(url, post_data, saveResponse)


            categories = []
            categories.push categoryId

            budgetModel = new __Model.Budget()

            if budgetID
                console.log "update"
                budgetModel = __Model.Budget.findBy "id", budgetID


            newLimit = parseFloat(cleanAmount)
            budgetModel.limit= newLimit
            budgetModel.limitString = Helpers.getFormatNumber(newLimit, currencySymbol)            
            budgetModel.left = Helpers.getFormatNumber(newLimit - budgetModel.spendingFloat, currencySymbol)            
            budgetModel.progress = (budgetModel.spendingFloat*100)/newLimit            
            budgetModel.leftText = l("%budget_left")
            budgetModel.color = "#ECAA23"
            if budgetModel.progress < 90
                budgetModel.color = "#A8C656"
            else if budgetModel.progress > 100
                budgetModel.progress = 100
                budgetModel.color = "#D14836"
                budgetModel.leftText = l("%budget_over")            
            budgetModel.tag_list= tags.split(",")
            budgetModel.categories= categories
            budgetModel.save()
            console.log budgetModel

            Lungo.Data.Storage.persistent("newBudgetUID", budgetModel.uid) 

            if budgetID
                Lungo.Data.Storage.persistent("newBudgetUID", null)                         
            Lungo.Router.back()             
        else
            Lungo.Notification.error(
                "Error",
                Helpers.getTranslate("budget_not_amount"),
                "cancel",
                3,
                null
            );

    onChangeCategory: (event) ->
        console.log "onChangeCategory"
        Lungo.dom("#category_control_back").val('section#newBudget > article div.category img')
        Lungo.Router.section("newCategory")
    

    tapNewAmount: (event) ->
        console.log "tapNewAmount"
        
        user = User.getUserInfo()
        Lungo.dom("#screen")[0].textContent = '0'+ user.currency.decimal_mark + '00'
        Lungo.dom("#decimal_mark")[0].textContent = user.currency.decimal_mark
        Lungo.dom("#control_back").val('section#newBudget > article div.amount span')
        Lungo.Router.section("newAmount")
        

    onLoadNewBudget: (event) ->
        console.log "onLoadNewBudget"
        
        budgetID = Lungo.Data.Storage.persistent("budgetID")

        console.log budgetID


        @title_budget[0].innerHTML = l("%budget")
        @budget_tags[0].placeholder = l("%budget_tags.placeholder")  
        @budget_cancel[0].innerHTML = l("%cancel")
        @budget_accept[0].innerHTML = l("%accept")        

        categoryClass = Lungo.dom("img#budget_category")[0].className.split(" ")
        categoryId = Helpers.getCategoryId(categoryClass[2])

        Lungo.dom("div#budget_tags")[0].innerHTML = ""
        Lungo.dom("ul#budget_bullets")[0].innerHTML = ""

        
        bulletsList = ""
        bulletsList = "<li class='prev'><a href='#' data-action='carousel' data-direction='left' data-icon='left' onclick=carouselBudget.prev()><span class='icon left'></span></a></li>"

        tags = __Model.Tag.all()
        tagsList = ""
        first = true

        tagsByCategory = (item.description for item in tags when item.category is categoryId)
        tagsByCategory = tagsByCategory.sort()
        numberTagsByCategory = tagsByCategory.length


        while tagsByCategory.length
            smallTagsList = tagsByCategory.splice(0,6)
            if first
                bulletsList += "<li class='selected'><span class='bullet'>o</span></li>"
            else
                bulletsList += "<li class=''><span class='bullet'>o</span></li>"
            first = false
            tagsList += "<div align='center'>"            
            for tag in smallTagsList
                tagsList += "<a href='#' class='button-tag' onclick=\"var tagInput = document.getElementById('budget_txt-tag'); if(tagInput.value.indexOf('"+tag+", ') === -1){ tagInput.value+='"+tag+", ';}else{tagInput.value = tagInput.value.replace('"+tag+", ','');} \">"+ tag + "</a>"                
            tagsList += '</div>'

        Lungo.dom("ul#budget_bullets")[0].style.cssText = ""
        if numberTagsByCategory < 6        
            Lungo.dom("ul#budget_bullets")[0].style.cssText = "display:none"
        else
            Lungo.dom("ul#budget_bullets")[0].style.cssText = "display: block;margin:10px"

        bulletsList += "<li class='next'><a href='#' data-action='carousel' data-direction='right' data-icon='right' onclick=carouselBudget.next()><span class='icon right'></span></a></li>"
        Lungo.dom("ul#budget_bullets")[0].innerHTML = bulletsList
        Lungo.dom("div#budget_tags")[0].innerHTML = tagsList

    saveResponse= (response) ->
        console.log "saveResponse"    

        budgetUID = Lungo.Data.Storage.persistent("newBudgetUID")

        if budgetUID isnt null
            budget = __Model.Budget.find(budgetUID)
            budget.id = response._id
            budget.save()        


            Lungo.Data.Storage.persistent("budgetID", budget.id)
            Lungo.Data.Storage.persistent("budgetUID", budget.uid)
            console.log budget.id


    errorResponse= (type, xhr) ->
        console.log "errorResponse"

        Lungo.Notification.hide()

        Lungo.Notification.error(
            "Error",
            Helpers.getTranslate("budget_error"),
            "cancel",
            3,
            null
        );
        

controller_newBudget = new NewBudgetCtrl "section#newBudget"