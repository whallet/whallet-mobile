class SessionCtrl extends Monocle.Controller

    events:
        "load section#home"                     : "onLoadHome"
        "load section#login"                    : "onLoadLogin"
        "load section#signup"                   : "onLoadSignup"
        "tap a[data-action=login]"              : "onLogin"
        "tap a[data-action=logout]"             : "onLogout"
        "tap a[data-action=create-account]"     : "onCreateAccount"
        "tap a[data-action=recovery]"           : "onRecoveryPassword"
        "tap a[data-action=recovery-password"   : "onSendPassword"


    elements:
        "#login_email"      : "login_email"
        "#login_password"   : "login_password"
        "#signup_email"     : "signup_email"
        "#signup_password"  : "signup_password"
        "#recovery_email"   : "recovery_email"

    onSendPassword: (event) ->
        console.log "onSendPassword"
        url = "http://www.whallet.com/api/v1/user/password.json"
        post_data = email: @recovery_email.val()
        Lungo.Service.Settings.error = errorRecoveryPassowrdResponse

        result = Lungo.Service.post(url, post_data, sendPasswordResponse)

    sendPasswordResponse= (response) ->
        console.log "sendPasswordResponse"

        Lungo.Notification.success(
            Helpers.getTranslate('email_found'),
            Helpers.getTranslate("succes_restore_password"),
            "check",
            3,
            afterNotification 
        )

    afterNotification= () ->
        Lungo.Router.back()

    onRecoveryPassword: (event) ->
        console.log "onRecoveryPassword"
        #Lungo.Router.section("recovery")


        template = "<form style=\"width: 100%\">"
        template += "<fieldset style=\"border-bottom: 2px solid #0093D5\">"
        template += "<span id=\"screen-input\" style=\"font-size: 1.4em; height:40px; margin-top:15px\" class=\"text thin left\">"
        template += "<abbr><input type=\"text\" id=\"emailRecovery\" placeholder=\"#{Helpers.getTranslate("txtemail")}\"></abbr>"
        template += "</span></fieldset></form>"

        Lungo.Notification.confirm({
            icon: '',
            title: Helpers.getTranslate("description_recovery"),
            description: template,
            accept: {
                icon: 'checkmark',
                label: Helpers.getTranslate("accept"),
                callback: ->
                    inputScreen = Lungo.dom("#emailRecovery")[0].value
                    url = "http://www.whallet.com/api/v1/user/password.json"
                    post_data = email: inputScreen

                    Lungo.Service.Settings.error = errorRecoveryPassowrdResponse

                    result = Lungo.Service.post(url, post_data, sendPasswordResponse)

            },
            cancel: {
                icon: 'close',
                label: Helpers.getTranslate("cancel"),
                callback: null
            }
        });


    onLoadSignup: (event) ->
        console.log "onLoadSignup"

    onLoadLogin: (event) ->
        console.log "onLoadLogin"
        tokenStored = Lungo.Data.Storage.persistent("tokenAuth")

        if tokenStored isnt null

            #user = Lungo.Data.Storage.persistent("user")
            #Lungo.dom("aside#options > header div.title")[0].innerHTML = user

            # Recovery tags
            url = "http://www.whallet.com/api/v1/helper/tags.json"
            post_data = token: tokenStored
            result = Lungo.Service.post(url, post_data, helperTagsResponse)

            Lungo.Router.section("monitor")

    onLoadHome: (event) ->
        console.log "onLoadHome"

    onCreateAccount: (event) ->
        console.log "onCreateAccount"

        url = "http://www.whallet.com/api/v1/registrations.json"
        post_data =
            email: @signup_email.val(),
            password: @signup_password.val(),
            password_confirmation: @signup_password.val(),
            locale: String.locale

        Lungo.Service.Settings.error = errorResponse
        result = Lungo.Service.post(url, post_data, registrationResponse)

    onLogout: (event) ->
        Lungo.Data.Storage.persistent("tokenAuth",null)
        Lungo.Data.Storage.persistent("user",null)
        Lungo.Data.Storage.persistent("userInfo",null)
        Lungo.Data.Storage.persistent("balances", null)
        Lungo.Data.Storage.persistent("movements", null)
        Lungo.Data.Storage.persistent("expenses", null)
        Lungo.Data.Storage.persistent("movementID", null)
        Lungo.Data.Storage.persistent("movementUID", null)
        Lungo.Data.Storage.persistent("budgetID", null)
        Lungo.Data.Storage.persistent("budgetUID", null)  
        __Model.Movement.destroyAll()
        __Model.Spending.destroyAll()
        __Model.Balance.destroyAll()
        __Model.Tag.destroyAll()
        __Model.Budget.destroyAll()

        Lungo.dom("section#login #login_email").val("")
        Lungo.dom("section#login #login_password").val("")

        Lungo.Router.section("home")

    onLogin: (event) ->
        console.log "onLogin"
        tokenStored = Lungo.Data.Storage.persistent("tokenAuth")

        Lungo.Notification.show()
        if tokenStored == null            
            url = "http://www.whallet.com/api/v1/tokens.json"
            post_data = email: @login_email.val(), password: @login_password.val()
            Lungo.Service.Settings.error = errorResponse

            result = Lungo.Service.post(url, post_data, sessionResponse)

        else
            Lungo.Data.Storage.persistent("tokenAuth",null)
            Lungo.Data.Storage.persistent("user",null)
            Lungo.Data.Storage.persistent("userInfo",null)
            Lungo.Data.Storage.persistent("balances", null)
            Lungo.Data.Storage.persistent("movements", null)
            Lungo.Data.Storage.persistent("expenses", null)
            Lungo.Data.Storage.persistent("movementID", null)
            Lungo.Data.Storage.persistent("movementUID", null)
            Lungo.Data.Storage.persistent("budgetID", null)
            Lungo.Data.Storage.persistent("budgetUID", null)            
            Lungo.Data.Storage.persistent("budgets", null)
            __Model.Budget.destroyAll()
            __Model.Movement.destroyAll()
            __Model.Spending.destroyAll()
            __Model.Balance.destroyAll()
            __Model.Tag.destroyAll()
            
            Lungo.Router.section("monitor")

    errorResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr)
        Lungo.Notification.error(
            Helpers.getTranslate("error"),
            Helpers.getTranslate("login_error"),
            "cancel",
            3,
            null
        );

    errorRecoveryPassowrdResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr)
        Lungo.Notification.error(
            Helpers.getTranslate("email_not_found"),
            Helpers.getTranslate("something_wrong"),
            "cancel",
            3,
            null
        );


    registrationResponse= (response) ->
        console.log "registrationResponse"

        console.log response
        url = "http://www.whallet.com/api/v1/tokens.json"
        post_data =
            email: Lungo.dom("section#signup #signup_email").val(),
            password: Lungo.dom("section#signup #signup_password").val()

        result = Lungo.Service.post(url, post_data, sessionResponse)

    sessionResponse= (response) ->
        console.log "sessionResponse"                

        if response.token?
            date = new Date()
            Lungo.Data.Storage.persistent("dateSelected", null)
            Lungo.Data.Storage.persistent("dateSelected", date)

            Lungo.Data.Storage.persistent("tokenAuth", response.token)
            email = Lungo.dom("section#login #login_email").val()
            Lungo.Data.Storage.persistent("user", email)

            #Lungo.dom("aside#options > header div.title")[0].innerHTML = email

            # Recovery tags
            url = "http://www.whallet.com/api/v1/helper/tags.json"
            post_data = token: response.token
            result = Lungo.Service.post(url, post_data, helperTagsResponse)

            # User info
            url = "http://www.whallet.com/api/v1/user/info.json"
            post_data = token: response.token
            result = Lungo.Service.post(url, post_data, userInfoResponse)

            #  2 - Spending -> Storage
            url = "http://www.whallet.com/api/v1/dashboard/balance.json"
            post_data = token: response.token
            result = Lungo.Service.post(url, post_data, balanceResponse)

            #  4- Movements -> Storage
            Lungo.Data.Storage.persistent("movements", null)
            url = "http://www.whallet.com/api/v1/movements.json"

            post_data =
                token: response.token,
                year: date.getFullYear(),
                month: date.getMonth()+1

            result = Lungo.Service.post(url, post_data, movementsResponse)

            # 5- Budgets

            Lungo.Data.Storage.persistent("budgets", null)
            url = "http://www.whallet.com/api/v1/budget/list.json"

            post_data = 
                token: response.token

            result = Lungo.Service.get(url, post_data, budgetsResponse)            




        else
            Lungo.Notification.error(
                Helpers.getTranslate("error"),
                Helpers.getTranslate("login_error"),
                "cancel",
                3,
                null
            );


    movementsResponse= (response) ->
        console.log "Splash.movementsResponse"        

        WhalletMovements.loadFromResponse response    

    budgetsResponse= (response) ->
        console.log "Splash.budgetsResponse"
        WhalletBudgets.loadFromResponse response          

    balanceResponse= (response) ->
        console.log "Splash.balanceResponse"

        __Model.Balance.destroyAll()

        for dataResponse in response
            balanceModel = new __Model.Balance
            balanceModel.date = dataResponse.date
            balanceModel.expense = dataResponse.data.expense
            balanceModel.income = dataResponse.data.income
            balanceModel.balance = dataResponse.data.balance  

            balanceModel.save()  

        Lungo.Router.section("monitor")

    helperTagsResponse= (response) ->
        console.log "helperTagsResponse"

        for dataResponse in response
            for tag in dataResponse.tags
                tagModel = __Model.Tag.create
                    description: tag.name,
                    category: dataResponse.category

    beforeSendUserInfo= (request) ->
        console.log "beforeSendUserInfo"

    userInfoResponse= (response) ->
        console.log "userInfoResponse"    

        if response?
            String.locale = response.locale
            Helpers.updateAllTranslateElement()
            Lungo.Data.Storage.persistent("userInfo", null)
            Lungo.Data.Storage.persistent("userInfo", response)    

            if User.isPro()                
                Lungo.dom('li#donate').addClass("hidden")    
                
            Android.hideAd();

            # Currencies
            Lungo.Data.Storage.persistent("currencies", null)
            url = "http://www.whallet.com/api/v1/helper/currencies.json"
            post_data = 
                locale: response.locale   
            result = Lungo.Service.get(url, post_data, currenciesResponse)  

    currenciesResponse= (response) ->
        console.log "currenciesResponse"
        Lungo.Data.Storage.persistent("currencies", response)

contoller_login = new SessionCtrl "section#recovery"
controller_login = new SessionCtrl "section#login"
controller_signup = new SessionCtrl "section#signup"
controller_home = new SessionCtrl "section#home"
controller_splash = new SessionCtrl "section#splash"
controller_aside = new SessionCtrl "aside#options"
