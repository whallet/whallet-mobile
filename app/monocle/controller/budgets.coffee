class BudgetsCtrl extends Monocle.Controller

    events:
        "load section#budgets"  : "onLoadBudgets"
        "tap a#newBudget"       : "onNewBudget"


    elements:
        "#newBudget"            : "newBudget"
        "#budgets_date"         : "budgets_date"
        "#title_budgets"        : "title_budgets"
        "#budget_days"          : "budget_days"

    onNewBudget: (event) ->
        console.log "onNewBudget"
        
        if User.isPro() or __Model.Budget.all().length < 1                        
            Lungo.Data.Storage.persistent("newBudgetUID", null)
            Lungo.Data.Storage.persistent("budgetID", null)

            Lungo.dom("section#newBudget > article div.category img")[0].src = "assets/images/categories/wtag.png";
            Lungo.dom("section#newBudget > article div.category img").removeClass();
            Lungo.dom("section#newBudget > article div.category img").addClass("icon whallet tag-2");    

            user = User.getUserInfo()
            Lungo.dom("section#newBudget > article div.amount span")[0].textContent = Helpers.userCurrencySymbol() + '0'+ user.currency.decimal_mark + '00'
            Lungo.dom("#budget_txt-tag").val("")   

            Lungo.Router.section("newBudget");            
        else            
            Lungo.Notification.html(Helpers.getTemplateNotProBudget(), "Close");

    onLoadBudgets: (event) ->
        console.log "onLoadBudgets"

        @title_budgets[0].innerHTML = l("%budgets")
        @newBudget[0].innerHTML = l("%budget_new")
        console.log @newBudget[0].innerHTML

        date = new Date(Lungo.Data.Storage.persistent("dateSelected"))
        @budgets_date[0].innerHTML = Helpers.getFormatDate(date)


        today = new Date()

        days_until_end_month = "0"
        if date.getFullYear() is today.getFullYear() and date.getMonth() is today.getMonth()            
            last_day_of_month = new Date(date.getFullYear(), date.getMonth()+1, 0).getDate()
            days_until_end_month = last_day_of_month - today.getDate()            
        
        @budget_days[0].innerHTML = l("%budget_finalize").replace("{{time}}", days_until_end_month)

        $$("ul#budgets").html(' ')

        currencySymbol = Helpers.userCurrencySymbol();
        budgets = __Model.Budget.all()
        movements = __Model.Movement.all()        
        for budget in budgets
            budget.movements = (item for item in movements when item.categoryID in budget.categories)            
            budget.movements = (item for item in budget.movements when item.tags[0] in budget.tag_list)
            
            spending = 0
            budget.movements.forEach (item) -> spending += item.amountDouble
            budget.spendingFloat = spending
            budget.spending = Helpers.getFormatNumber(spending, currencySymbol)

            left = budget.limit - spending

            budget.left = Helpers.getFormatNumber(left, currencySymbol)
            
            budget.progress = (spending*100)/budget.limit
            budget.limitString = Helpers.getFormatNumber(budget.limit, currencySymbol)

            budget.leftText = l("%budget_left")
            budget.color = "#ECAA23"
            if budget.progress < 90
                budget.color = "#A8C656"
            else if budget.progress > 100
                budget.progress = 100
                budget.color = "#D14836"
                budget.leftText = l("%budget_over")

            budget.description = l("%category_"+budget.categories[0])
            budget.description_tags = budget.tag_list.join()
            budget.save()
            
            view = new __View.BudgetItemList model: budget
            view.append budget



controller_budgets = new BudgetsCtrl "section#budgets"
