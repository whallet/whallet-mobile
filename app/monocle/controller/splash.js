// Generated by CoffeeScript 1.7.1
(function() {
  var SplashCtrl, controller_splash,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  SplashCtrl = (function(_super) {
    var balanceResponse, budgetsResponse, currenciesResponse, errorResponse, helperTagsResponse, movementsResponse, userInfoResponse;

    __extends(SplashCtrl, _super);

    function SplashCtrl() {
      return SplashCtrl.__super__.constructor.apply(this, arguments);
    }

    SplashCtrl.prototype.events = {
      "load section#splash": "onLoadSplash"
    };

    SplashCtrl.prototype.onLoadSplash = function(event) {
      var balances, budgets, date, locale, post_data, result, tags, tokenStored, url, userInfo;
      console.log("onLoadSplash");
      date = new Date();
      Lungo.Data.Storage.persistent("dateSelected", null);
      Lungo.Data.Storage.persistent("movementID", null);
      Lungo.Data.Storage.persistent("movementUID", null);
      Lungo.Data.Storage.persistent("budgetID", null);
      Lungo.Data.Storage.persistent("budgetUID", null);
      Lungo.Data.Storage.persistent("nuevo-movementUID", null);
      Lungo.Data.Storage.persistent("dateSelected", date);
      Lungo.dom('li#donate').removeClass("hidden");
      tokenStored = Lungo.Data.Storage.persistent("tokenAuth");
      if (tokenStored !== null) {
        if (User.isPro()) {
          Lungo.dom('li#donate').addClass("hidden");
        }
        Android.hideAd();
        userInfo = Lungo.Data.Storage.persistent("userInfo");
        console.log(userInfo);
        if (userInfo !== null) {
          String.locale = User.getUserInfo().locale;
          Helpers.updateAllTranslateElement();
        }
        tags = Lungo.Data.Storage.persistent("tags");
        if (tags !== null) {
          tags = WhalletTags.loadFromStorage();
        }
        balances = Lungo.Data.Storage.persistent("balances");
        if (balances !== null) {
          balances = WhalletBalances.loadFromStorage();
          Lungo.Router.section("monitor");
        }
        url = "http://www.whallet.com/api/v1/dashboard/balance.json";
        post_data = {
          token: tokenStored
        };
        result = Lungo.Service.post(url, post_data, balanceResponse);
        Lungo.Data.Storage.persistent("movements", null);
        url = "http://www.whallet.com/api/v1/movements.json";
        post_data = {
          token: tokenStored,
          year: date.getFullYear(),
          month: date.getMonth() + 1
        };
        result = Lungo.Service.post(url, post_data, movementsResponse);
        budgets = Lungo.Data.Storage.persistent("budgets");
        if (budgets !== null) {
          budgets = WhalletBudgets.loadFromStorage();
        }
        url = "http://www.whallet.com/api/v1/budget/list.json";
        post_data = {
          token: tokenStored
        };
        result = Lungo.Service.get(url, post_data, budgetsResponse);
        url = "http://www.whallet.com/api/v1/user/info.json";
        post_data = {
          token: tokenStored
        };
        result = Lungo.Service.post(url, post_data, userInfoResponse);
        url = "http://www.whallet.com/api/v1/helper/tags.json";
        post_data = {
          token: tokenStored
        };
        result = Lungo.Service.post(url, post_data, helperTagsResponse);
        Lungo.Data.Storage.persistent("currencies", null);
        url = "http://www.whallet.com/api/v1/helper/currencies.json";
        userInfo = User.getUserInfo();
        locale = "es";
        if (userInfo !== null) {
          locale = User.getUserInfo().locale;
        }
        post_data = {
          locale: locale
        };
        return result = Lungo.Service.get(url, post_data, currenciesResponse);
      } else {
        Lungo.Data.Storage.persistent("token", null);
        Lungo.Data.Storage.persistent("tokenAuth", null);
        Lungo.Data.Storage.persistent("user", null);
        Lungo.Data.Storage.persistent("userInfo", null);
        Lungo.Data.Storage.persistent("balances", null);
        Lungo.Data.Storage.persistent("movements", null);
        Lungo.Data.Storage.persistent("expenses", null);
        Lungo.Data.Storage.persistent("budgets", null);
        __Model.Movement.destroyAll();
        __Model.Spending.destroyAll();
        __Model.Balance.destroyAll();
        __Model.Tag.destroyAll();
        return Lungo.Router.section("home");
      }
    };

    errorResponse = function(type, xhr) {
      console.log("errorResponse");
      console.log("xhr: %o", xhr);
      return Lungo.Notification.error(Helpers.getTranslate("error_unknown"), Helpers.getTranslate("check_inet"), "broadcast", 5, null);
    };

    movementsResponse = function(response) {
      console.log("Splash.movementsResponse");
      return WhalletMovements.loadFromResponse(response);
    };

    budgetsResponse = function(response) {
      var budget, dataResponse, _i, _len;
      console.log("Splash.budgetsResponse");
      __Model.Budget.destroyAll();
      for (_i = 0, _len = response.length; _i < _len; _i++) {
        dataResponse = response[_i];
        budget = new __Model.Budget;
        budget.id = dataResponse._id;
        budget.description = dataResponse.description ? dataResponse.description : l('%no_description');
        budget.limit = dataResponse.limit;
        budget.tag_list = dataResponse.tag_list;
        budget.categories = dataResponse.categories;
        budget.save();
      }
      return WhalletBudgets.saveToStorage();
    };

    balanceResponse = function(response) {
      var balance, balanceModel, dataResponse, _i, _len;
      console.log("Splash.balanceResponse");
      balance = Lungo.Data.Storage.persistent("balances");
      __Model.Balance.destroyAll();
      for (_i = 0, _len = response.length; _i < _len; _i++) {
        dataResponse = response[_i];
        balanceModel = new __Model.Balance;
        balanceModel.date = dataResponse.date;
        balanceModel.expense = dataResponse.data.expense;
        balanceModel.income = dataResponse.data.income;
        balanceModel.balance = dataResponse.data.balance;
        balanceModel.save();
      }
      WhalletBalances.saveToStorage();
      if (balance === null) {
        return Lungo.Router.section("monitor");
      }
    };

    helperTagsResponse = function(response) {
      var dataResponse, tag, tagModel, _i, _j, _len, _len1, _ref;
      console.log("helperTagsResponse");
      __Model.Tag.destroyAll();
      for (_i = 0, _len = response.length; _i < _len; _i++) {
        dataResponse = response[_i];
        _ref = dataResponse.tags;
        for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
          tag = _ref[_j];
          tagModel = __Model.Tag.create({
            description: tag.name,
            category: dataResponse.category
          });
        }
      }
      return WhalletTags.saveToStorage();
    };

    currenciesResponse = function(response) {
      console.log("currenciesResponse");
      return Lungo.Data.Storage.persistent("currencies", response);
    };

    userInfoResponse = function(response) {
      console.log("userInfoResponse");
      if (response != null) {
        console.log(response);
        String.locale = response.locale;
        Helpers.updateAllTranslateElement();
        Lungo.Data.Storage.persistent("userInfo", null);
        Lungo.Data.Storage.persistent("userInfo", response);
        return Android.hideAd();
      }
    };

    return SplashCtrl;

  })(Monocle.Controller);

  controller_splash = new SplashCtrl("section#splash");

}).call(this);
