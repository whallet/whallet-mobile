class BudgetDetailCtrl extends Monocle.Controller

    events:
        "load section#budget_detail"            : "onLoadBudgetDetail"
        "tap a[data-action=delete]"             : "onDelete"
        "tap a[data-action=edit]"               : "onEdit"

    elements:
        "#title_budget_detail"                  : "title_budget_detail"    
        "#title_movements_included"             : "title_movements_included"  
        "#title_detail_daily"                   : "title_detail_daily"
        "#budget_detail_description"            : "budget_detail_description"
        "#budget_detail_description_tags"       : "budget_detail_description_tags"
        "#budget_detail_pending"                : "budget_detail_pending"
        "#budget_detail_pending_destripcion"    : "budget_detail_pending_destripcion"
        "li#budget_detail_info"                 : "budget_detail_info"
        "#budget_detail_spent"                  : "budget_detail_spent"
        "#budget_detail_limit"                  : "budget_detail_limit"
        "#budget_detail_spent_desc"             : "budget_detail_spent_desc"
        "#budget_detail_limit_desc"             : "budget_detail_limit_desc"

    onEdit: (event) ->
        console.log "BudgetDetailCtrl.onEdit"

        budgetID = Lungo.Data.Storage.persistent("budgetID")

        if budgetID
            budget = __Model.Budget.findBy  "id", budgetID

            Lungo.Data.Storage.persistent("newBudgetUID", null)

            categoryName = Helpers.getCategoryName budget.categories[0]
            Lungo.dom("section#newBudget > article div.category img").removeClass();
            Lungo.dom("section#newBudget > article div.category img").addClass("icon whallet "+ categoryName);

            if budget.category == "tag-2"
                categoryName = "wtag"
            Lungo.dom("section#newBudget > article div.category img")[0].src = "assets/images/categories/"+categoryName+".png";
            

            user = User.getUserInfo()
            amountClean = budget.limitString.replace(" " + currencySymbol, "")
            amountClean = amountClean.replace(currencySymbol + " ", "")
            Lungo.dom("section#newBudget > article div.amount span")[0].textContent = Helpers.userCurrencySymbol() + ' '+ amountClean
            
            Lungo.dom("#budget_txt-tag").val(budget.description_tags)   

            Lungo.Router.section("newBudget");
        else
            Lungo.Notification.error(
                Helpers.getTranslate('error'),
                Helpers.getTranslate("error_msg"),
                "check",
                3,
                null 
            )

    onDelete: (event) ->
        console.log "BudgetDetailCtrl.onDelete"

        budgetID = Lungo.Data.Storage.persistent("budgetID")

        if budgetID
            url = "http://www.whallet.com/api/v1/budget/delete.json"

            post_data = 
                token: Lungo.Data.Storage.persistent("tokenAuth"),
                id: budgetID

            Lungo.Service.Settings.error = errorResponse
            result = Lungo.Service.post(url, post_data, deleteResponse)

            for budgetAux in __Model.Budget.all()
                if budgetAux.id == budgetID
                    budgetAux.destroy()

            Lungo.Notification.success(
                Helpers.getTranslate('success'),
                Helpers.getTranslate("budget_deleted"),
                 "check",
                1,
                afterNotification 
            )
        else
            Lungo.Notification.error(
                Helpers.getTranslate('error'),
                Helpers.getTranslate("error_msg"),
                "check",
                3,
                null 
            )

    afterNotification= () ->
        Lungo.Router.back()

    deleteResponse= (response) ->
        console.log "BudgetDetailCtrl.deleteResponse"
        Lungo.Data.Storage.persistent("budgetID", null)

    onLoadBudgetDetail: (event) ->
        console.log "onLoadBudgetDetail"

        @title_budget_detail[0].innerHTML = l("%budget")
        @title_movements_included[0].innerHTML = l("%budget_movements_included")
        @title_detail_daily[0].innerHTML = l("%budget_detail_daily")
    
        # clear list
        $$("ul#budget_detail_movements").html(' ')
        
        budgetUID = Lungo.Data.Storage.persistent("budgetUID")
        budget = __Model.Budget.find(budgetUID)
        
        @budget_detail_description[0].innerHTML = budget.description + ": " 
        @budget_detail_description_tags[0].innerHTML = budget.description_tags
        @budget_detail_pending[0].innerHTML = budget.left
        @budget_detail_pending_destripcion[0].innerHTML = budget.leftText
        @budget_detail_spent[0].innerHTML = budget.spending
        @budget_detail_limit[0].innerHTML = budget.limitString
        @budget_detail_spent_desc[0].innerHTML = l("%budget_spent")
        @budget_detail_limit_desc[0].innerHTML = l("%budget_limit")        

        @budget_detail_info[0].style.backgroundColor = budget.color

        daily_expenses_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        movements = budget.movements

        movements.sort (a, b) -> 
            a = new Date(a.date)
            b = new Date(b.date)

            res = 0
            if a < b
                res = 1
            if a > b
                res = -1

            res  
        
        for movement in movements
            position = movement.date.getDate() - 1
            daily_expenses_data[position] = daily_expenses_data[position] + movement.amountDouble


            movement.formatDate = Helpers.getLongFormatDate(movement.date)
            view = new __View.BudgetMovementIncludedList model: movement
            view.append movement

        ## daily expenses

        budget_detail_daily = Lungo.dom("canvas#budget_detail_daily_chart")[0]
        budget_detail_daily_container_canvas = Lungo.dom("section#budget_detail div#main_daily")[0]
        budget_detail_daily.width = budget_detail_daily_container_canvas.clientWidth
        budget_detail_daily.height = budget_detail_daily_container_canvas.clientHeight

        budget_detail_daily_chart = (budget_detail_daily).getContext("2d")

        budget_detail_daily_data = {
            labels : ["1","","3","","5","","7","","9","","11","","13","","15","","17","","19","","21","","23","","25","","27","","29","","31"],
            datasets : [
                {
                    fillColor : budget.color,
                    data : daily_expenses_data
                }
            ]
        }

        optionsBar = {
            scaleOverlay : false,
            scaleOverride : false,
            scaleLineColor : "rgba(0,0,0,.1)",
            scaleLineWidth : 1,
            scaleShowLabels : true,
            scaleLabel : "<%=value%>",
            scaleFontFamily : "'Arial'",
            scaleFontSize : 8,
            scaleFontStyle : "normal",
            scaleFontColor : "#666",    
            scaleShowGridLines : false,
            scaleGridLineColor : "rgba(0,0,0,.05)",
            scaleGridLineWidth : 1, 
            barShowStroke : false,
            barStrokeWidth : 0,
            barValueSpacing : 0,
            barDatasetSpacing : 0,
            animation : true,
            animationSteps : 60,
            animationEasing : "easeOutQuart",
            onAnimationComplete : null
            
        }

        budget_detail_daily_bar = new Chart(budget_detail_daily_chart).Bar(budget_detail_daily_data, optionsBar);        


        ###
        canvas = Lungo.dom("canvas#budget_detail_chart")[0]
        container_canvas = Lungo.dom("section#budget_detail div#main")[0]
        canvas.width = container_canvas.clientWidth
        canvas.height = container_canvas.clientHeight

        chart = (canvas).getContext("2d")

        optionsChart = {
            segmentShowStroke : true,
            segmentStrokeColor : "#fff",
            segmentStrokeWidth : 2,
            percentageInnerCutout : 75,
            animation : true,
            animationSteps : 100,
            animationEasing : "easeOutBounce",
            animateRotate : true,
            animateScale : false,
            onAnimationComplete : null
        }
        
        budgetData = []
        
        data = {
            value: parseFloat(40),
            color: "#A8C656"
        }

        budgetData.push data

        data = {
            value: parseFloat(60),
            color: "#595959"
        }

        

        budgetData.push data
        myDoughnut = new Chart(chart).Pie(budgetData, optionsChart)

        budget_detail_daily = Lungo.dom("canvas#budget_detail_daily_chart")[0]
        budget_detail_daily_container_canvas = Lungo.dom("section#budget_detail div#main_daily")[0]
        budget_detail_daily.width = budget_detail_daily_container_canvas.clientWidth
        budget_detail_daily.height = budget_detail_daily_container_canvas.clientHeight

        budget_detail_daily_chart = (budget_detail_daily).getContext("2d")

        budget_detail_daily_data = {
            labels : ["1","","3","","5","","7","","9","","11","","13","","15","","17","","19","","21","","23","","25","","27","","29","","31"],
            datasets : [
                {
                    fillColor : "rgba(220,220,220,0.5)",
                    strokeColor : "rgba(220,220,220,1)",
                    data : [80,150,90,100,175,55,40,0,20,0,10,200,100,0,0,200,100,65,0,100,20,0,20,0,0,0,0,0,0,0,0]
                }
            ]
        }

        optionsBar = {
            scaleOverlay : false,
            scaleOverride : false,
            scaleLineColor : "rgba(0,0,0,.1)",
            scaleLineWidth : 1,
            scaleShowLabels : true,
            scaleLabel : "<%=value%>",
            scaleFontFamily : "'Arial'",
            scaleFontSize : 8,
            scaleFontStyle : "normal",
            scaleFontColor : "#666",    
            scaleShowGridLines : false,
            scaleGridLineColor : "rgba(0,0,0,.05)",
            scaleGridLineWidth : 1, 
            barShowStroke : false,
            barStrokeWidth : 0,
            barValueSpacing : 0,
            barDatasetSpacing : 0,
            animation : true,
            animationSteps : 60,
            animationEasing : "easeOutQuart",
            onAnimationComplete : null
            
        }

        budget_detail_daily_bar = new Chart(budget_detail_daily_chart).Bar(budget_detail_daily_data, optionsBar);
        ###

controller_budgetDetail = new BudgetDetailCtrl "section#budget_detail"