class AsideCtrl extends Monocle.Controller

    events:
        "load aside#options"    :  "onLoadAside"
        "tap li"                :  "onChangeSection"

    elements:
        "#aside_my_whallet"     :   "my_whallet"
        "#aside_distribution"   :   "distribution"
        "#aside_movements"      :   "movements"
        "#aside_budgets"        :   "budgets"
        "#aside_settings"       :   "settings"
        "#aside_share"          :   "share"
        "#aside_donate"          :  "be_pro"
        "#button_signout"       :   "button_signout"

    onLoadAside: (event) ->
        console.log "onLoadAside"

        @aside_my_whallet[0].innerHTML = l("%my_whallet")
        @aside_distribution[0].innerHTML = l("%distribution")
        @aside_movements[0].innerHTML = l("%movements")
        @aside_budgets[0].innerHTML = l("%budgets")
        @aside_settings[0].innerHTML = l("%settings")
        @aside_share[0].innerHTML = l("%share")
        console.log @aside_donate
        console.log l("%be_pro")
        @aside_donate[0].innerHTML = l("%be_pro")

    onChangeSection: (event) ->
        console.log "onChangeSection"

        Lungo.dom("li.active").removeClass()
        Lungo.dom("li#"+event.currentTarget.id).addClass('active')

        Lungo.Router.section(checkSectionNoData(event.currentTarget.id))

    checkSectionNoData= (section) ->
        if section == "movements"
            movements = Lungo.Data.Storage.persistent("movements")

            #if movements isnt null and movements.length == 0
            #    Lungo.Data.Storage.persistent("sectionReturnNoData", section)
            #    section = "emptyData"            
        else if section == "distribution"
            distribution = Lungo.Data.Storage.persistent("expenseDistribution")

            #if distribution isnt null and distribution.data.length == 0
            #    Lungo.Data.Storage.persistent("sectionReturnNoData", section)
            #    section = "emptyData"            
        else if section == "monitor" 
            balances = Lungo.Data.Storage.persistent("balances")

            #if balances is null
            #    Lungo.Data.Storage.persistent("sectionReturnNoData", section)
            #    section = "emptyData"        

        Lungo.Router.aside(section, "options");

        section

controller_aside = new AsideCtrl "aside#options"