class DistributionCtrl extends Monocle.Controller

    events:
        "load section#distribution"     : "onLoadDistribution"
        "tap h4#distribution_date"           : "onChangeDate"
        "tap span.icon.menu"            : "onChangeDate"     

    elements:
        "#distribution_date"            :   "distribution_date"
        "#distribution_expense_month"   :   "distribution_expense_month"
        "#distribution_data_table"      :   "distribution_data_table"
        "#title_distribution"           :   "title_distribution"
        "#title_expenses"                :   "title_expenses"

    onChangeDate: (event) ->
        console.log "onChangeDate"

        fechaCalendario = new Date(Lungo.Data.Storage.persistent("dateSelected"))

        Lungo.Notification.confirm({
            icon: '',
            title: Helpers.getTranslate('select_date'),
            description: Helpers.getTemplateSelectMonthYear(fechaCalendario),
            accept: {
                icon: 'checkmark',
                label: Helpers.getTranslate('accept'),
                callback: (results) ->
                    console.log "accept onChangeDate" 
                    date = Calendar.getActualDate()
                    Lungo.Data.Storage.persistent("movements", null)
                    Lungo.Data.Storage.persistent("dateSelected", null)
                    Lungo.Data.Storage.persistent("dateSelected", date)

                    $$("h4#distribution_date")[0].innerHTML = Helpers.getFormatDate(date)   

                    Lungo.Notification.show()
                    url = "http://www.whallet.com/api/v1/movements.json"
                    post_data =
                        token: Lungo.Data.Storage.persistent("tokenAuth"),
                        year: date.getFullYear(),
                        month: date.getMonth()+1

                    result = Lungo.Service.post(url, post_data, movementsServiceResponse)
            },
            cancel: {
                icon: 'close',
                label: Helpers.getTranslate('cancel'),
                callback: null
            }
        })

    movementsServiceResponse= (response) ->
        console.log "movementsServiceResponse"
        WhalletMovements.loadFromResponse response  

        Lungo.Notification.hide()
        loadDistribution __Model.Movement.all()

    onLoadDistribution: (event) ->
        console.log "onLoadDistribution"

        @title_distribution[0].innerHTML = l("%distribution")
        @title_expenses[0].innerHTML = l("%expenses")
        date = new Date(Lungo.Data.Storage.persistent("dateSelected"))
        @distribution_date[0].innerHTML = Helpers.getFormatDate(date)

        loadDistribution __Model.Movement.all()

    loadDistribution= (movements) ->
        console.log "LoadDistribution"
        __Model.Spending.destroyAll()
        currencySymbol = Helpers.userCurrencySymbol()

    
        totalCategory_0 = 0
        totalCategory_1 = 0
        totalCategory_2 = 0
        totalCategory_3 = 0
        totalCategory_4 = 0
        totalCategory_5 = 0
        totalCategory_6 = 0
        totalCategory_7 = 0
        totalCategory_8 = 0
        totalCategory_9 = 0        
        total = 0

        for movement in __Model.Movement.all()
            if movement.categoryID == 0
                totalCategory_0 += movement.amountDouble
            if movement.categoryID == 1
                totalCategory_1 += movement.amountDouble
            if movement.categoryID == 2
                totalCategory_2 += movement.amountDouble
            if movement.categoryID == 3
                totalCategory_3 += movement.amountDouble
            if movement.categoryID == 4
                totalCategory_4 += movement.amountDouble                                                                
            if movement.categoryID == 5
                totalCategory_5 += movement.amountDouble
            if movement.categoryID == 6
                totalCategory_6 += movement.amountDouble
            if movement.categoryID == 7
                totalCategory_7 += movement.amountDouble
            if movement.categoryID == 8
                totalCategory_8 += movement.amountDouble
            if movement.categoryID == 9
                totalCategory_9 += movement.amountDouble          
                  

        total = totalCategory_0 + totalCategory_1 + totalCategory_2 + totalCategory_3 + totalCategory_4 + totalCategory_5 + totalCategory_6 + totalCategory_7 + totalCategory_8 + totalCategory_9
        count = 0
        if totalCategory_0 > 0
            spendingModel = __Model.Spending.create
                category: Helpers.getCategoryName('0'),
                description: Helpers.getTranslate('category_0'),
                amountDouble: totalCategory_0,
                amount:  Helpers.getFormatNumber(totalCategory_0, currencySymbol),                
                percentage: ((totalCategory_0*100)/total).toFixed(2) + " %",
                percentageDouble: ((totalCategory_0*100)/total).toFixed(2),            
                position: count
            count = count + 1

        if totalCategory_1 > 0
            spendingModel = __Model.Spending.create
                category: Helpers.getCategoryName('1'),
                description: Helpers.getTranslate('category_1'),
                amountDouble: totalCategory_1,                
                amount:  Helpers.getFormatNumber(totalCategory_1, currencySymbol),
                percentage: ((totalCategory_1*100)/total).toFixed(2) + " %",
                percentageDouble: ((totalCategory_1*100)/total).toFixed(2),
                position: count  
            count = count + 1

        if totalCategory_2 > 0
            spendingModel = __Model.Spending.create
                category: Helpers.getCategoryName('2'),
                description: Helpers.getTranslate('category_2'),
                amountDouble: totalCategory_2,
                amount:  Helpers.getFormatNumber(totalCategory_2, currencySymbol),
                percentage: ((totalCategory_2*100)/total).toFixed(2) + " %",
                percentageDouble: ((totalCategory_2*100)/total).toFixed(2),
                position: count  
            count = count + 1

        if totalCategory_3 > 0
            spendingModel = __Model.Spending.create
                category: Helpers.getCategoryName('3'),
                description: Helpers.getTranslate('category_3'),
                amountDouble: totalCategory_3,
                amount:  Helpers.getFormatNumber(totalCategory_3, currencySymbol),
                percentage: ((totalCategory_3*100)/total).toFixed(2) + " %",
                percentageDouble: ((totalCategory_3*100)/total).toFixed(2),
                position: count
            count = count + 1

        if totalCategory_4 > 0
            spendingModel = __Model.Spending.create
                category: Helpers.getCategoryName('4'),
                description: Helpers.getTranslate('category_4'),
                amountDouble: totalCategory_4,
                amount:  Helpers.getFormatNumber(totalCategory_4, currencySymbol),
                percentage: ((totalCategory_4*100)/total).toFixed(2) + " %",
                percentageDouble: ((totalCategory_4*100)/total).toFixed(2),                
                position: count
            count = count + 1

        if totalCategory_5 > 0
            spendingModel = __Model.Spending.create
                category: Helpers.getCategoryName('5'),
                description: Helpers.getTranslate('category_5'),
                amountDouble: totalCategory_5,
                amount:  Helpers.getFormatNumber(totalCategory_5, currencySymbol),
                percentage: ((totalCategory_5*100)/total).toFixed(2) + " %",
                percentageDouble: ((totalCategory_5*100)/total).toFixed(2),
                position: count  
            count = count + 1

        if totalCategory_6 > 0
            spendingModel = __Model.Spending.create
                category: Helpers.getCategoryName('6'),
                description: Helpers.getTranslate('category_6'),
                amountDouble: totalCategory_6,
                amount:  Helpers.getFormatNumber(totalCategory_6, currencySymbol),
                percentage: ((totalCategory_6*100)/total).toFixed(2) + " %",
                percentageDouble: ((totalCategory_6*100)/total).toFixed(2),
                position: count
            count = count + 1

        if totalCategory_7 > 0
            spendingModel = __Model.Spending.create
                category: Helpers.getCategoryName('7'),
                description: Helpers.getTranslate('category_7'),
                amountDouble: totalCategory_7,
                amount:  Helpers.getFormatNumber(totalCategory_7, currencySymbol),
                percentage: ((totalCategory_7*100)/total).toFixed(2) + " %",
                percentageDouble: ((totalCategory_7*100)/total).toFixed(2),
                position: count
            count = count + 1

        if totalCategory_8 > 0
            spendingModel = __Model.Spending.create
                category: Helpers.getCategoryName('8'),
                description: Helpers.getTranslate('category_8'),
                amountDouble: totalCategory_8,
                amount:  Helpers.getFormatNumber(totalCategory_8, currencySymbol),
                percentage: ((totalCategory_8*100)/total).toFixed(2) + " %",
                percentageDouble: ((totalCategory_8*100)/total).toFixed(2),
                position: count
            count = count + 1

        if totalCategory_9 > 0
            spendingModel = __Model.Spending.create
                category: Helpers.getCategoryName('9'),
                description: Helpers.getTranslate('category_9'),
                amountDouble: totalCategory_9,
                amount:  Helpers.getFormatNumber(totalCategory_9, currencySymbol),
                percentage: ((totalCategory_9*100)/total).toFixed(2) + " %",
                percentageDouble: ((totalCategory_9*100)/total).toFixed(2),                
                position: count
            count = count + 1                

        spendings = __Model.Spending.all()


        spendings.sort (a, b) -> 
            a = new Date(a.amountDouble)
            b = new Date(b.amountDouble)
            b-a

        count = 0
        for s in spendings
            s.updateAttributes position: count
            count = count + 1

        spendingResponse spendings

    spendingResponse= (response) ->
        console.log "spendingResponse"

        dateSelected = new Date(Lungo.Data.Storage.persistent("dateSelected"))

        # clear list
        $$("ul#distribution_table").html(' ')
        $$("h1#expense_month").html(' ')

        if response.length == 0
            $$("div#distribution_no_data").removeClass()
            $$("div#distribution_data").removeClass()
            $$("div#distribution_data").addClass("hidden")
            @distribution_data_table.className = "hidden"
            @distribution_date.innerHTML = Helpers.getFormatDate(dateSelected)
        else
            $$("div#distribution_no_data").removeClass()
            $$("div#distribution_data").removeClass()
            $$("div#distribution_no_data").addClass("hidden")
            @distribution_data_table.className = ""

            colors= new Array("#E3563A","#A8C656","#19AA5F","#00AFD8","#514E93","#E25695","#C35049","#E66F59","#EBA543",'#D9D6D1','#D4D84F','#7ABA5C','#68B0A2','#5E8BB4','#C3C2BD','#8D8D8B','#5D5F5A','#B04B73','#C1B295','#977550')
            currencySymbol = Helpers.userCurrencySymbol()

            @distribution_date.innerHTML = Helpers.getFormatDate(dateSelected)
            

            count = 0
            distributionData = []
            total = 0
            for spendingModel in response                
                total += spendingModel.amountDouble

                view = new __View.SpendingItem model: spendingModel                
                view.append spendingModel

                data = {
                    value: parseFloat(spendingModel['percentageDouble']),
                    color: colors[count]
                }
                distributionData.push data

                count = count + 1

            @distribution_expense_month.innerHTML =  Helpers.getFormatNumber(total, currencySymbol)

            canvas = Lungo.dom("canvas#distribution_chart")[0]
            container_canvas = Lungo.dom("section#distribution div#main")[0]
            canvas.width = container_canvas.clientWidth
            canvas.height = container_canvas.clientHeight

            chart = (canvas).getContext("2d")

            optionsChart = {
                animation: false
            }
            
            myDoughnut = new Chart(chart).Pie(distributionData, optionsChart)

        Lungo.Notification.hide()

controller_distribution = new DistributionCtrl "section#distribution"