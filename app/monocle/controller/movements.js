// Generated by CoffeeScript 1.7.1
(function() {
  var MovementsCtrl, controller_movements,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  MovementsCtrl = (function(_super) {
    var deleteResponse, errorResponse, movementsResponse, movementsSearchService, movementsServiceResponse;

    __extends(MovementsCtrl, _super);

    MovementsCtrl.prototype.events = {
      "load section#movements": "onLoadMovements",
      "tap h4#movements_date": "onChangeDate",
      "tap span.icon.menu": "onChangeDate",
      "tap h4#movements_search": "onSearch",
      "tap h4#movements_clear": "onClearFilter"
    };

    MovementsCtrl.prototype.elements = {
      "#movements_date": "movements_date",
      "#title_movements": "title_movements",
      "#movements_clear": "movements_clear"
    };

    function MovementsCtrl() {
      MovementsCtrl.__super__.constructor.apply(this, arguments);
      __Model.Movement.bind("destroy", this.bindMovementDestroy);
    }

    MovementsCtrl.prototype.onClearFilter = function() {
      Lungo.dom("#movements_clear").removeClass();
      Lungo.dom("#movements_clear").addClass("hidden");
      return movementsSearchService("", "");
    };

    MovementsCtrl.prototype.onSearch = function() {
      console.log("onSearch");
      if (User.isPro()) {
        return Lungo.Notification.confirm({
          icon: '',
          title: l("%popup_search_movements"),
          description: Helpers.getTemplateSearch(),
          accept: {
            icon: 'checkmark',
            label: Helpers.getTranslate('accept'),
            callback: function(results) {
              var description_filter, tags_filter;
              Lungo.dom("#movements_clear").removeClass();
              description_filter = Lungo.dom("#search_description")[0].value;
              tags_filter = Lungo.dom("#search_tags")[0].value;
              return movementsSearchService(description_filter, tags_filter);
            }
          },
          cancel: {
            icon: 'close',
            label: Helpers.getTranslate('cancel'),
            callback: null
          }
        });
      } else {
        return Lungo.Notification.html(Helpers.getTemplateNotProSearchMovements(), "Close");
      }
    };

    MovementsCtrl.prototype.bindMovementDestroy = function(movement) {
      return console.log("bindMovementDestroy");

      /*
      onlyRemove = Lungo.Data.Storage.persistent("onlyRemove", "true")
      
      if onlyRemove is null
      
          url = "http://www.whallet.com/api/v1/movement/delete.json"
          date = new Date()
          post_data =
              token: Lungo.Data.Storage.persistent("tokenAuth"),
              id: movement.id
      
          Lungo.Service.Settings.error = errorResponse
          result = Lungo.Service.post(url, post_data, deleteResponse)
      Lungo.Data.Storage.persistent("onlyRemove", null)
       */
    };

    errorResponse = function(type, xhr) {
      console.log("errorResponse");
      console.log("xhr: %o", xhr);
      return Lungo.Notification.error("Error", Helpers.getTranslate("error_msg"), "cancel", 3, null);
    };

    deleteResponse = function(response) {
      console.log("deleteResponse");
      Lungo.Data.Storage.persistent("balances", null);
      Lungo.Data.Storage.persistent("movements", null);
      return Lungo.Notification.success("Success", Helpers.getTranslate("movement_deleted"), "check", 3, null);
    };

    MovementsCtrl.prototype.onChangeDate = function(event) {
      var fechaCalendario;
      console.log("onChangeDate");
      fechaCalendario = new Date(Lungo.Data.Storage.persistent("dateSelected"));
      return Lungo.Notification.confirm({
        icon: '',
        title: Helpers.getTranslate("select_date"),
        description: Helpers.getTemplateSelectMonthYear(fechaCalendario),
        accept: {
          icon: 'checkmark',
          label: Helpers.getTranslate('accept'),
          callback: function(results) {
            var date, post_data, result, url;
            console.log("accept onChangeDate");
            date = Calendar.getActualDate();
            Lungo.Data.Storage.persistent("dateSelected", null);
            Lungo.Data.Storage.persistent("dateSelected", date);
            $$("h4#movements_date")[0].innerHTML = Helpers.getFormatDate(date);
            Lungo.Notification.show();
            url = "http://www.whallet.com/api/v1/movements.json";
            post_data = {
              token: Lungo.Data.Storage.persistent("tokenAuth"),
              year: date.getFullYear(),
              month: date.getMonth() + 1
            };
            return result = Lungo.Service.post(url, post_data, movementsServiceResponse);
          }
        },
        cancel: {
          icon: 'close',
          label: Helpers.getTranslate('cancel'),
          callback: null
        }
      });
    };

    movementsSearchService = function(description, tags) {
      var movements;
      console.log("movementsSearchService");
      movements = __Model.Movement.all();
      movements.sort(function(a, b) {
        var res;
        a = new Date(a.date);
        b = new Date(b.date);
        res = 0;
        if (a < b) {
          res = 1;
        }
        if (a > b) {
          res = -1;
        }
        return res;
      });
      Lungo.Notification.hide();
      return movementsResponse(movements, description, tags);
    };

    movementsServiceResponse = function(response) {
      var movements;
      WhalletMovements.loadFromResponse(response);
      movements = __Model.Movement.all();
      movements.sort(function(a, b) {
        var res;
        a = new Date(a.date);
        b = new Date(b.date);
        res = 0;
        if (a < b) {
          res = 1;
        }
        if (a > b) {
          res = -1;
        }
        return res;
      });
      Lungo.Notification.hide();
      return movementsResponse(movements);
    };

    MovementsCtrl.prototype.onLoadMovements = function(event) {
      var date, movements;
      console.log("onLoadMovements");
      Lungo.dom("#movements_clear").removeClass();
      Lungo.dom("#movements_clear").addClass("hidden");
      this.movements_clear[0].innerHTML = l("%clear_filter");
      this.title_movements[0].innerHTML = l("%movements");
      date = Lungo.Data.Storage.persistent("dateSelected");
      this.movements_date[0].innerHTML = Helpers.getFormatDate(new Date(date));
      Lungo.Data.Storage.persistent("movementID", null);
      Lungo.Data.Storage.persistent("movementUID", null);
      movements = __Model.Movement.all();
      movements.sort(function(a, b) {
        var res;
        a = new Date(a.date);
        b = new Date(b.date);
        res = 0;
        if (a < b) {
          res = 1;
        }
        if (a > b) {
          res = -1;
        }
        return res;
      });
      return movementsResponse(movements);
    };

    movementsResponse = function(response, filter_description, filter_tags) {
      var actualFormatDate, currencySymbol, f_description, filter_tags_list, formatDate, m_description, movement, searchResultByDescription, searchResultByTags, tag_filter, view, viewHeader, _i, _j, _len, _len1;
      console.log("movementsResponse");
      currencySymbol = Helpers.userCurrencySymbol();
      $$("ul#movements").html(' ');
      $$("div#distribution_no_data").addClass("hidden");
      $$("article#movements").addClass("indented");
      if (movements.length === 0) {
        $$("div#distribution_no_data").removeClass();
        $$("article#movements").removeClass("indented");
      } else {

      }
      formatDate = "";
      for (_i = 0, _len = response.length; _i < _len; _i++) {
        movement = response[_i];
        m_description = movement.description.toUpperCase();
        f_description = filter_description;
        if (filter_description) {
          filter_description = filter_description.toUpperCase();
        }
        searchResultByDescription = m_description.search(f_description);
        searchResultByTags = 0;
        if (filter_tags != null) {
          filter_tags_list = filter_tags.split(',');
          for (_j = 0, _len1 = filter_tags_list.length; _j < _len1; _j++) {
            tag_filter = filter_tags_list[_j];
            searchResultByTags = movement.tags[0].search(tag_filter.replace(/^\s+|\s+$/g, ""));
            if (searchResultByTags >= 0) {
              break;
            }
          }
        }
        if (searchResultByDescription >= 0 && searchResultByTags >= 0) {
          actualFormatDate = movement['formatDate'];
          if (formatDate !== actualFormatDate) {
            formatDate = actualFormatDate;
            viewHeader = new __View.MovementHeaderList({
              model: movement
            });
            viewHeader.append(movement);
          }
          view = new __View.MovementItemList({
            model: movement
          });
          view.append(movement);
        }
      }
      return Lungo.Notification.hide();
    };

    return MovementsCtrl;

  })(Monocle.Controller);

  controller_movements = new MovementsCtrl("section#movements");

}).call(this);
