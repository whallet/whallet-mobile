class MonitorCtrl extends Monocle.Controller

    events:
        "load section#monitor"  : "onLoadMonitor"
        "tap h4#monitor_date"   : "onChangeDate"
        "tap span.icon.menu"    : "onChangeDate"

    elements:
        "#monitor_date"     :   "monitor_date"
        "#balance_month"    :   "balance_month"
        "#expense_month"    :   "expense_month"
        "#income_month"     :   "income_month"
        "#bar"              :   "bar"
        "#title_my_whallet" :   "title_my_whallet"
        "#title_balance"    :   "title_balance"

    onChangeDate: (event) ->
        console.log "onChangeDate"

        fechaCalendario = new Date(Lungo.Data.Storage.persistent("dateSelected"))

        Lungo.Notification.confirm({
            icon: '',
            title: Helpers.getTranslate('select_date'),
            description: Helpers.getTemplateSelectMonthYear(fechaCalendario),
            accept: {
                icon: 'checkmark',
                label: Helpers.getTranslate('accept'),
                callback: (results) ->
                    console.log "accept onChangeDate" 
                    date = Calendar.getActualDate()
                    Lungo.Data.Storage.persistent("movements", null)
                    Lungo.Data.Storage.persistent("dateSelected", null)
                    Lungo.Data.Storage.persistent("dateSelected", date)

                    $$("h4#monitor_date")[0].innerHTML = Helpers.getFormatDate(date)
                    balanceResponse __Model.Balance.all()

                    Lungo.Notification.show()
                    url = "http://www.whallet.com/api/v1/movements.json"
                    post_data =
                        token: Lungo.Data.Storage.persistent("tokenAuth"),
                        year: date.getFullYear(),
                        month: date.getMonth()+1

                    result = Lungo.Service.post(url, post_data, movementsServiceResponse)
            },
            cancel: {
                icon: 'close',
                label: Helpers.getTranslate('cancel'),
                callback: null
            }
        })

    movementsServiceResponse= (response) ->
        __Model.Movement.destroyAll()
        WhalletMovements.loadFromResponse response

    onLoadMonitor: (event) ->
        console.log "onLoadMonitor"

        @title_my_whallet[0].innerHTML = l("%my_whallet")
        @title_balance[0].innerHTML = l("%balance")

        date = new Date(Lungo.Data.Storage.persistent("dateSelected"))

        @monitor_date[0].innerHTML = Helpers.getFormatDate(date)

        balanceResponse __Model.Balance.all()

    balanceResponse= (response) ->
        console.log "balanceResponse"

        currencySymbol = Helpers.userCurrencySymbol()
        
        m_names = new Array(Helpers.getTranslate("abbr_month_names_1"),
        Helpers.getTranslate("abbr_month_names_2"),
        Helpers.getTranslate("abbr_month_names_3"),
        Helpers.getTranslate("abbr_month_names_4"),
        Helpers.getTranslate("abbr_month_names_5"),
        Helpers.getTranslate("abbr_month_names_6"),
        Helpers.getTranslate("abbr_month_names_7"),
        Helpers.getTranslate("abbr_month_names_8"),
        Helpers.getTranslate("abbr_month_names_9"),
        Helpers.getTranslate("abbr_month_names_10"),
        Helpers.getTranslate("abbr_month_names_11"),
        Helpers.getTranslate("abbr_month_names_12"))

        incomes = []
        expenses = []
        dates = []
        
        noData = 0     
        date = new Date(Lungo.Data.Storage.persistent("dateSelected"))
        for dataBalance in response
            dateSplit = dataBalance.date.split('-')
            dateBalance = new Date(dateSplit[0], dateSplit[1] - 1, dateSplit[2])

            if((dateBalance.getMonth() == date.getMonth()) and (dateBalance.getFullYear() == date.getFullYear()))

                @balance_month.innerHTML = Helpers.getFormatNumber(dataBalance.balance, currencySymbol)
                if dataBalance.balance > 0
                    @balance_month.innerHTML = "+"+ Helpers.getFormatNumber(dataBalance.balance, currencySymbol)

                @expense_month.innerHTML = "<span class='icon whallet arrow-down-2 red'></span>" + Helpers.getFormatNumber(dataBalance.expense, currencySymbol)
                @income_month.innerHTML = "<span class='icon whallet arrow-up green'></span>"  + Helpers.getFormatNumber(dataBalance.income, currencySymbol)

                if dataBalance.balance <= 0
                    percentage_income = 0
                else                    
                    percentage_income = (dataBalance.balance*100)/dataBalance.income
                @bar.style.width = percentage_income + "%";

            dates.push m_names[dateBalance.getMonth()]
            incomes.push dataBalance.income
            expenses.push dataBalance.expense  

            noData += dataBalance.income + dataBalance.expense
        
        if noData > 0
            $$("div#evolution_no_data").addClass("hidden")
            $$("div#evolutionChart").removeClass()
            
            lineChartData = {
                labels : dates,
                datasets : [
                    {
                        fillColor : "rgba(244,246,237,0.5)",
                        strokeColor : "rgba(168,198,86,1)",
                        pointColor : "rgba(168,198,86,1)",
                        pointStrokeColor : "#fff",
                        data : incomes
                    },
                    {
                        fillColor : "rgba(247,236,235,0.5)",
                        strokeColor : "rgba(209,72,54,1)",
                        pointColor : "rgba(209,72,54,1)",
                        pointStrokeColor : "#fff",
                        data : expenses
                    }
                ]
            }

            optionsChart = {
                scaleOverlay: true,
                bezierCurve: false,
                animation: false
            }
            canvas = Lungo.dom("#canvas")[0]
            container_canvas = Lungo.dom("#evolutionChart")[0]
            canvas.width = container_canvas.clientWidth
            canvas.height = container_canvas.clientHeight

            chart = (Lungo.dom("canvas#canvas")[0]).getContext("2d")
            myLine = new Chart(chart).Line(lineChartData, optionsChart)
        else
            $$("div#evolutionChart").addClass("hidden")
            $$("div#evolution_no_data").removeClass()

        Lungo.Notification.hide()

controller_monitor = new MonitorCtrl "section#monitor"