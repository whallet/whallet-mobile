class LocalesCtrl extends Monocle.Controller
    events:
        "load section#locales"  :   "onLoadLocales"
        "tap li#es"   :   "onChangeLocaleES"
        "tap li#en"   :   "onChangeLocaleEN"
        "tap li#zh"   :   "onChangeLocaleZH"
        "tap li#ko"   :   "onChangeLocaleKO"
        "tap li#ja"   :   "onChangeLocaleJA"
        "tap li#ru"   :   "onChangeLocaleRU"
    elements:
        "#title_locales"    : "title_locales"
        "#text_es"  : "text_es"
        "#text_en"  : "text_en"
        "#text_zh"  : "text_zh"
        "#text_ko"  : "text_ko"
        "#text_ja"  : "text_ja"
        "#text_ru"  : "text_ru"


    onLoadLocales: (event) ->
        console.log "onLoadLocales"

        @title_locales[0].innerHTML = l("%locales")
        @text_es[0].innerHTML = l("%locale_es")
        @text_en[0].innerHTML = l("%locale_en")
        @text_zh[0].innerHTML = l("%locale_zh")
        @text_ko[0].innerHTML = l("%locale_ko")
        @text_ja[0].innerHTML = l("%locale_ja")
        @text_ru[0].innerHTML = l("%locale_ru")

        $$("li#es").removeClass()
        $$("li#en").removeClass()
        $$("li#zh").removeClass()
        $$("li#ko").removeClass()
        $$("li#ja").removeClass()
        $$("li#ru").removeClass()

        $$("li#"+User.getLocale()).addClass("active-item")

    onChangeLocaleES: (event) ->
        console.log "onChangeLocaleES"
        changeLocale("es")

    onChangeLocaleEN: (event) ->
        console.log "onChangeLocaleEN"
        changeLocale("en")

    onChangeLocaleZH: (event) ->
        console.log "onChangeLocaleZH"
        changeLocale("zh")

    onChangeLocaleKO: (event) ->
        console.log "onChangeLocaleKO"
        changeLocale("ko")

    onChangeLocaleJA: (event) ->
        console.log "onChangeLocaleJA"
        changeLocale("ja")

    onChangeLocaleRU: (event) ->
        console.log "onChangeLocaleRU"                
        changeLocale("ru")


    changeLocale= (locale) ->
        console.log "changeLocale"
        String.locale = locale
        userInfo = Lungo.Data.Storage.persistent("userInfo")      
        userInfo.locale = locale
        Lungo.Data.Storage.persistent("userInfo", userInfo)

        Helpers.updateAllTranslateElement()
      
        tokenStored = Lungo.Data.Storage.persistent("tokenAuth")
        url = "http://www.whallet.com/api/v1/user/update.json"
        post_data = 
            token: tokenStored,
            locale: locale
        Lungo.Service.Settings.error = errorResponse
        Lungo.Service.post(url, post_data, userUpdateLocaleResponse)

    userUpdateLocaleResponse= (response) ->
        console.log "userUpdateLocaleResponse"   
        Lungo.Data.Storage.persistent("userInfo", response)

        Lungo.Notification.success(
            Helpers.getTranslate("success"), 
            Helpers.getTranslate("locale_changed"), 
            "check", 
            1, 
            null)
        Lungo.Router.back()       

    errorResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr)
        Lungo.Notification.error(
            Helpers.getTranslate("error"),
            Helpers.getTranslate("error_msg"),
            "cancel",
            3,
            null
        )


controller_locales = new LocalesCtrl "section#locales"