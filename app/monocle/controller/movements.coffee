class MovementsCtrl extends Monocle.Controller

    events:
        "load section#movements"    : "onLoadMovements"
        "tap h4#movements_date"     : "onChangeDate"
        "tap span.icon.menu"        : "onChangeDate"
        "tap h4#movements_search"   : "onSearch"
        "tap h4#movements_clear"    : "onClearFilter"

    elements:
        "#movements_date"           :   "movements_date"
        "#title_movements"           :   "title_movements"
        "#movements_clear"           : "movements_clear"

    constructor: ->
        super
        __Model.Movement.bind "destroy",  @bindMovementDestroy

    onClearFilter: () ->
        Lungo.dom("#movements_clear").removeClass();
        Lungo.dom("#movements_clear").addClass("hidden");
        movementsSearchService "", ""

    onSearch: () ->
        console.log "onSearch"                

        if User.isPro()
            Lungo.Notification.confirm({
                icon: '',
                title: l("%popup_search_movements") ,
                description:  Helpers.getTemplateSearch(),
                accept: {
                    icon: 'checkmark',
                    label: Helpers.getTranslate('accept'),
                    callback: (results) ->
                        Lungo.dom("#movements_clear").removeClass();
                        description_filter =  Lungo.dom("#search_description")[0].value
                        tags_filter = Lungo.dom("#search_tags")[0].value

                        movementsSearchService description_filter, tags_filter
                                        
                },
                cancel: {
                    icon: 'close',
                    label: Helpers.getTranslate('cancel'),
                    callback: null
                }
            })        
        else
            Lungo.Notification.html(Helpers.getTemplateNotProSearchMovements(), "Close");

    bindMovementDestroy: (movement) ->
        console.log "bindMovementDestroy"
        #document.getElementById("delete_tag_"+ event.currentTarget.id).className = "right tag cancel"
        ###
        onlyRemove = Lungo.Data.Storage.persistent("onlyRemove", "true")

        if onlyRemove is null

            url = "http://www.whallet.com/api/v1/movement/delete.json"
            date = new Date()
            post_data =
                token: Lungo.Data.Storage.persistent("tokenAuth"),
                id: movement.id

            Lungo.Service.Settings.error = errorResponse
            result = Lungo.Service.post(url, post_data, deleteResponse)
        Lungo.Data.Storage.persistent("onlyRemove", null)
        ###

    errorResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr)
        Lungo.Notification.error(
            "Error",
            Helpers.getTranslate("error_msg"),
            "cancel",
            3,
            null
        )

    deleteResponse= (response) ->
        console.log "deleteResponse"

        Lungo.Data.Storage.persistent("balances", null)
        #Lungo.Data.Storage.persistent("expenseDistribution", null)
        Lungo.Data.Storage.persistent("movements", null)

        Lungo.Notification.success(
            "Success",
            Helpers.getTranslate("movement_deleted"),
            "check",
            3,
            null
        )

    onChangeDate: (event) ->
        console.log "onChangeDate"

        fechaCalendario = new Date(Lungo.Data.Storage.persistent("dateSelected"))

        Lungo.Notification.confirm({
            icon: '',
            title: Helpers.getTranslate("select_date"),
            description:  Helpers.getTemplateSelectMonthYear(fechaCalendario),
            accept: {
                icon: 'checkmark',
                label: Helpers.getTranslate('accept'),
                callback: (results) ->
                    console.log "accept onChangeDate" 
                    date = Calendar.getActualDate()
                    Lungo.Data.Storage.persistent("dateSelected", null)
                    Lungo.Data.Storage.persistent("dateSelected", date)

                    $$("h4#movements_date")[0].innerHTML = Helpers.getFormatDate(date)
                    
                    #Lungo.Data.Storage.persistent("movements", null)

                    Lungo.Notification.show()
                    url = "http://www.whallet.com/api/v1/movements.json"
                    post_data =
                        token: Lungo.Data.Storage.persistent("tokenAuth"),
                        year: date.getFullYear(),
                        month: date.getMonth()+1

                    result = Lungo.Service.post(url, post_data, movementsServiceResponse)
            },
            cancel: {
                icon: 'close',
                label: Helpers.getTranslate('cancel'),
                callback: null
            }
        })

    movementsSearchService= (description, tags) ->
        console.log "movementsSearchService"
        movements = __Model.Movement.all()

        #m =  movements.where description:"Jose"

        movements.sort (a, b) -> 
            a = new Date(a.date)
            b = new Date(b.date)

            res = 0
            if a < b
                res = 1
            if a > b
                res = -1

            res            

        Lungo.Notification.hide()

        movementsResponse movements, description, tags

    movementsServiceResponse= (response) ->
        WhalletMovements.loadFromResponse response

        movements = __Model.Movement.all()

        movements.sort (a, b) -> 
            a = new Date(a.date)
            b = new Date(b.date)

            res = 0
            if a < b
                res = 1
            if a > b
                res = -1

            res            

        Lungo.Notification.hide()

        movementsResponse movements

    onLoadMovements: (event) ->
        console.log "onLoadMovements"

        Lungo.dom("#movements_clear").removeClass();
        Lungo.dom("#movements_clear").addClass("hidden");

        @movements_clear[0].innerHTML = l("%clear_filter")        
        @title_movements[0].innerHTML = l("%movements")        

        date = Lungo.Data.Storage.persistent("dateSelected")
        @movements_date[0].innerHTML = Helpers.getFormatDate(new Date(date))

        Lungo.Data.Storage.persistent("movementID", null)
        Lungo.Data.Storage.persistent("movementUID", null)

        movements = __Model.Movement.all()           

        movements.sort (a, b) -> 
            a = new Date(a.date)
            b = new Date(b.date)

            res = 0
            if a < b
                res = 1
            if a > b
                res = -1

            res            

        movementsResponse movements

    movementsResponse= (response, filter_description, filter_tags) ->
        console.log "movementsResponse"
        currencySymbol = Helpers.userCurrencySymbol()       

        # clear list
        $$("ul#movements").html(' ')
        $$("div#distribution_no_data").addClass("hidden")
        $$("article#movements").addClass("indented")
        if movements.length == 0
            $$("div#distribution_no_data").removeClass()
            $$("article#movements").removeClass("indented")
        else

        formatDate = ""
        for movement in response                  
            
            m_description = movement.description.toUpperCase()
            f_description = filter_description
            if filter_description
                filter_description = filter_description.toUpperCase()

            searchResultByDescription = m_description.search f_description

            searchResultByTags = 0
            if filter_tags?
                filter_tags_list = filter_tags.split(',')                
                for tag_filter in filter_tags_list                    
                    searchResultByTags = movement.tags[0].search tag_filter.replace /^\s+|\s+$/g, ""                    
                    if searchResultByTags >= 0
                        break    

            if (searchResultByDescription) >= 0 and (searchResultByTags) >= 0              
                actualFormatDate = movement['formatDate']
                if formatDate isnt actualFormatDate
                    formatDate = actualFormatDate
                    viewHeader = new __View.MovementHeaderList model: movement
                    viewHeader.append movement

                view = new __View.MovementItemList model: movement
                view.append movement

        Lungo.Notification.hide()

controller_movements = new MovementsCtrl "section#movements"