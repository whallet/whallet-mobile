// Generated by CoffeeScript 1.6.3
(function() {
  var MonitorCtrl, controller_monitor, _ref,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  MonitorCtrl = (function(_super) {
    var balanceResponse, movementsServiceResponse;

    __extends(MonitorCtrl, _super);

    function MonitorCtrl() {
      _ref = MonitorCtrl.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    MonitorCtrl.prototype.events = {
      "load section#monitor": "onLoadMonitor",
      "tap h4#monitor_date": "onChangeDate",
      "tap span.icon.menu": "onChangeDate"
    };

    MonitorCtrl.prototype.elements = {
      "#monitor_date": "monitor_date",
      "#balance_month": "balance_month",
      "#expense_month": "expense_month",
      "#income_month": "income_month",
      "#bar": "bar",
      "#title_my_whallet": "title_my_whallet",
      "#title_balance": "title_balance"
    };

    MonitorCtrl.prototype.onChangeDate = function(event) {
      var fechaCalendario;
      console.log("onChangeDate");
      fechaCalendario = new Date(Lungo.Data.Storage.persistent("dateSelected"));
      return Lungo.Notification.confirm({
        icon: '',
        title: Helpers.getTranslate('select_date'),
        description: Helpers.getTemplateSelectMonthYear(fechaCalendario),
        accept: {
          icon: 'checkmark',
          label: Helpers.getTranslate('accept'),
          callback: function(results) {
            var date, post_data, result, url;
            console.log("accept onChangeDate");
            date = Calendar.getActualDate();
            Lungo.Data.Storage.persistent("movements", null);
            Lungo.Data.Storage.persistent("dateSelected", null);
            Lungo.Data.Storage.persistent("dateSelected", date);
            $$("h4#monitor_date")[0].innerHTML = Helpers.getFormatDate(date);
            balanceResponse(__Model.Balance.all());
            Lungo.Notification.show();
            url = "http://www.whallet.com/api/v1/movements.json";
            post_data = {
              token: Lungo.Data.Storage.persistent("tokenAuth"),
              year: date.getFullYear(),
              month: date.getMonth() + 1
            };
            return result = Lungo.Service.post(url, post_data, movementsServiceResponse);
          }
        },
        cancel: {
          icon: 'close',
          label: Helpers.getTranslate('cancel'),
          callback: null
        }
      });
    };

    movementsServiceResponse = function(response) {
      __Model.Movement.destroyAll();
      return WhalletMovements.loadFromResponse(response);
    };

    MonitorCtrl.prototype.onLoadMonitor = function(event) {
      var date;
      console.log("onLoadMonitor");
      this.title_my_whallet[0].innerHTML = l("%my_whallet");
      this.title_balance[0].innerHTML = l("%balance");
      date = new Date(Lungo.Data.Storage.persistent("dateSelected"));
      this.monitor_date[0].innerHTML = Helpers.getFormatDate(date);
      return balanceResponse(__Model.Balance.all());
    };

    balanceResponse = function(response) {
      var canvas, chart, container_canvas, currencySymbol, dataBalance, date, dateBalance, dateSplit, dates, expenses, incomes, lineChartData, m_names, myLine, noData, optionsChart, percentage_income, _i, _len;
      console.log("balanceResponse");
      currencySymbol = Helpers.userCurrencySymbol();
      m_names = new Array(Helpers.getTranslate("abbr_month_names_1"), Helpers.getTranslate("abbr_month_names_2"), Helpers.getTranslate("abbr_month_names_3"), Helpers.getTranslate("abbr_month_names_4"), Helpers.getTranslate("abbr_month_names_5"), Helpers.getTranslate("abbr_month_names_6"), Helpers.getTranslate("abbr_month_names_7"), Helpers.getTranslate("abbr_month_names_8"), Helpers.getTranslate("abbr_month_names_9"), Helpers.getTranslate("abbr_month_names_10"), Helpers.getTranslate("abbr_month_names_11"), Helpers.getTranslate("abbr_month_names_12"));
      incomes = [];
      expenses = [];
      dates = [];
      noData = 0;
      date = new Date(Lungo.Data.Storage.persistent("dateSelected"));
      for (_i = 0, _len = response.length; _i < _len; _i++) {
        dataBalance = response[_i];
        dateSplit = dataBalance.date.split('-');
        dateBalance = new Date(dateSplit[0], dateSplit[1] - 1, dateSplit[2]);
        if ((dateBalance.getMonth() === date.getMonth()) && (dateBalance.getFullYear() === date.getFullYear())) {
          this.balance_month.innerHTML = Helpers.getFormatNumber(dataBalance.balance, currencySymbol);
          if (dataBalance.balance > 0) {
            this.balance_month.innerHTML = "+" + Helpers.getFormatNumber(dataBalance.balance, currencySymbol);
          }
          this.expense_month.innerHTML = "<span class='icon whallet arrow-down-2 red'></span>" + Helpers.getFormatNumber(dataBalance.expense, currencySymbol);
          this.income_month.innerHTML = "<span class='icon whallet arrow-up green'></span>" + Helpers.getFormatNumber(dataBalance.income, currencySymbol);
          if (dataBalance.balance <= 0) {
            percentage_income = 0;
          } else {
            percentage_income = (dataBalance.balance * 100) / dataBalance.income;
          }
          this.bar.style.width = percentage_income + "%";
        }
        dates.push(m_names[dateBalance.getMonth()]);
        incomes.push(dataBalance.income);
        expenses.push(dataBalance.expense);
        noData += dataBalance.income + dataBalance.expense;
      }
      if (noData > 0) {
        $$("div#evolution_no_data").addClass("hidden");
        $$("div#evolutionChart").removeClass();
        lineChartData = {
          labels: dates,
          datasets: [
            {
              fillColor: "rgba(244,246,237,0.5)",
              strokeColor: "rgba(168,198,86,1)",
              pointColor: "rgba(168,198,86,1)",
              pointStrokeColor: "#fff",
              data: incomes
            }, {
              fillColor: "rgba(247,236,235,0.5)",
              strokeColor: "rgba(209,72,54,1)",
              pointColor: "rgba(209,72,54,1)",
              pointStrokeColor: "#fff",
              data: expenses
            }
          ]
        };
        optionsChart = {
          scaleOverlay: true,
          bezierCurve: false,
          animation: false
        };
        canvas = Lungo.dom("#canvas")[0];
        container_canvas = Lungo.dom("#evolutionChart")[0];
        canvas.width = container_canvas.clientWidth;
        canvas.height = container_canvas.clientHeight;
        chart = (Lungo.dom("canvas#canvas")[0]).getContext("2d");
        myLine = new Chart(chart).Line(lineChartData, optionsChart);
      } else {
        $$("div#evolutionChart").addClass("hidden");
        $$("div#evolution_no_data").removeClass();
      }
      return Lungo.Notification.hide();
    };

    return MonitorCtrl;

  })(Monocle.Controller);

  controller_monitor = new MonitorCtrl("section#monitor");

}).call(this);
