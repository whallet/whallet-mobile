class DonateCtrl extends Monocle.Controller

	events:
		"load section#donate"	: "onLoadDonate"
		"tap fieldset#btnBuy"            : "onStartBuy"     

	elements:
		"#title_donate"						: "title_donate"
		"#pro_header"						: "pro_header"
		"#pro_description"					: "pro_description"
		"#pro_price"						: "pro_price"
		"#pro_disclaimer"					: "pro_disclaimer"


	onStartBuy: (event) ->
		console.log "onStartBuy"

		tokenStored = Lungo.Data.Storage.persistent("tokenAuth")
		Android.startBuyProcess(tokenStored)

	onLoadDonate: (event) ->
		console.log "onLoadDonate"			

		@pro_header[0].innerHTML = l("%pro_page_title")
		@pro_description[0].innerHTML = l("%pro_page_description")		
		@pro_price[0].innerHTML = l("%pro_page_price")	
		@pro_disclaimer[0].innerHTML = l("%pro_page_info")
		@title_donate[0].innerHTML = l("%pro_title")


controller_donae = new DonateCtrl "section#donate"