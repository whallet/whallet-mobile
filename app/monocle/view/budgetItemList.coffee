class __View.BudgetItemList extends Monocle.View

    container: "ul#budgets"

    template:
        """
        <li id="budget_{{id}}" class="thumb big selectable" style="padding-bottom: 15px; margin-bottom: 0px">
            
            <div style="white-space: nowrap; overflow:hidden; text-overflow:ellipsis;"><strong ><span class="text bold" >{{description}}</span>: {{description_tags}}</strong></div>
            <form>
            <div class="progress">
                <span class="bar" style="background-color: #DDDDDD;height: 20px !important;">
                    <span id="budget_1_bar" class="value" style="background-color: {{color}}; width: {{progress}}%;"></span>
                </span>
            </div>
            </form>
            <small>
                {{spending}} of {{limitString}}
                <div class="right text "><span class="text bold">{{left}}</span> {{leftText}}</div>
            </small>

        </li>
        """

    events:
        "tap li"    : "onViewDetail"

    onViewDetail: (event) ->
        console.log "onViewDetail"

        Lungo.Data.Storage.persistent("budgetID", @model.id)
        Lungo.Data.Storage.persistent("budgetUID", @model.uid)
        Lungo.Router.section("budget_detail")
