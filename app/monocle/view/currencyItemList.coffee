class __View.CurrencyItemList extends Monocle.View

    container: "ul#currencies"

    template:
        """
        <li id="currency_{{id}}" style="{{#active}} background-color: #AFD4D4;color: white !important{{/active}}">
            <div class="right"><h1><span class="text bold" style="line-height: 30px;">{{symbol}}</span></h1></div>
            <strong><span class="text bold">{{name}}</span></strong>
            <small>ej: {{example}}</small>
        </li>
        """

    events:
        "tap li"        : "onSelect"

    onSelect: (event) ->
        console.log "onSelect.view"

        currencyActive = User.getCurrencyActive()

        if currencyActive._id != @model.id
            Lungo.Notification.show()
            tokenStored = Lungo.Data.Storage.persistent("tokenAuth")
            url = "http://www.whallet.com/api/v1/user/update.json"
            post_data = 
                token: tokenStored,
                currency_id: @model.id
            Lungo.Service.Settings.error = errorResponse
            result = Lungo.Service.post(url, post_data, userUpdateResponse)
        else
            Lungo.Router.back()

    userUpdateResponse= (response) ->
        console.log "userUpdateResponse"            
        Lungo.Data.Storage.persistent("userInfo", response)

        Lungo.Notification.success(
            Helpers.getTranslate("success"),
            Helpers.getTranslate("currency_changed"),
            "check",
            3,
            null
        ) 
        Lungo.Router.back()       

    errorResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr)
        Lungo.Notification.error(
            Helpers.getTranslate("error"),
            Helpers.getTranslate("error_msg"),
            "cancel",
            3,
            null
        )