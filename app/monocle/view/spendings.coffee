class __View.SpendingItem extends Monocle.View

    container: "ul#distribution_table"

    template:
        """
            <li style="" class="thumb big text-color_{{position}}">                
                <img src="assets/images/categories/{{category}}.png">                   
                <strong style="display:block">
                        <div class="right" style="width: 60px; text-align:right">{{percentage}}</div>
                        <div class="right" style="">{{amount}}</div>                        
                    </div>                    
                </strong>
                <strong>{{description}}</strong>
                <small>&nbsp</small>                
            </li>
        """