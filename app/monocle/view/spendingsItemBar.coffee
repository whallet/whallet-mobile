class __View.SpendingItemBar extends Monocle.View

    container: "ul.bargraph"

    template:
        """
        <li class="color_{{position}}" style="width:{{percentage}}%"></li>
        """