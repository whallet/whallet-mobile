class __View.MovementTagList extends Monocle.View

	container: "div#tags"

	template:
		"""
		<a href="#" class="button">{{description}}</a>
        """