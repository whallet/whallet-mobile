class __View.BudgetMovementIncludedList extends Monocle.View

		container: "ul#budget_detail_movements"

		template:
			"""
	            <li class="thumb big selectable" style="padding-bottom: 15px; margin-bottom: 0px">
	                <img src="assets/images/categories/{{category}}.png">
	                <div class="right text {{type}}"><h4>{{amount}}</h4></div>
	                <strong><span class="text bold">{{tags}}</span></strong>
	                <small>
	                    {{description}}
	                </small>
	              	<div class="right"><small>{{formatDate}}</small><span>
	            </li>
			"""