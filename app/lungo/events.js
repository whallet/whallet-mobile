var decimalAdded = false;
var MAX_DECIMAL = 2;
var countDecimal = 0;

Lungo.Events.init({
    'tap a#newMovement': function(){
        console.log("NewMovement");

        
        Lungo.dom("a#deleteMovement").removeClass();
        Lungo.dom("a#deleteMovement").addClass("hidden");
        Lungo.Data.Storage.persistent("movementID", null);
        Lungo.dom("a#income").removeClass();
        Lungo.dom("a#expense").removeClass();
        Lungo.dom("a#expense").addClass("active-expense");
        Lungo.dom("img#category")[0].src = "assets/images/categories/wtag.png";
        Lungo.dom("img#category").removeClass();
        Lungo.dom("img#category").addClass("icon whallet tag-2");        

        __Model.MovementRecurrence.destroyAll();

        recurrence = __Model.MovementRecurrence.create({
          rule: __Model.MovementRecurrence.RULES.NEVER,
          rule_locale: Helpers.getTranslate("recurrence_"+__Model.MovementRecurrence.RULES.NEVER),
          endson: "",
          endson_input : ""
        });

        Lungo.dom("section#NewMovement > article input#movement_recurrence").val(recurrence.uid);    
        Lungo.dom("section#NewMovement > article span.reload")[0].innerHTML = "";        


        currency = Helpers.userCurrencySymbol();
        user = Lungo.Data.Storage.persistent("userInfo");

        Lungo.dom("section#NewMovement > article span#amount")[0].innerText = currency + " 0" + user.currency.decimal_mark + "00";
        Lungo.dom("section#NewMovement > article input#txt-description")[0].value = "";
        Lungo.dom("section#NewMovement > article input#txt-tag")[0].value = "";

        currentDate = new Date();
        dd = currentDate.getDate();
        mm = currentDate.getMonth() + 1;
        yyyy = currentDate.getFullYear();
        if (dd < 10) {
            dd = "0" + dd;
        }
        if (mm < 10) {
            mm = "0" + mm;
        }
        currentDate = dd + '/' + mm + '/' + yyyy;
        Lungo.dom("section#NewMovement > article input#txt-date")[0].value = currentDate;

        Lungo.Data.Storage.persistent("movementID", null)
        Lungo.Data.Storage.persistent("movementUID", null)

        Lungo.dom("#control_back").val('section#NewMovement > article div.amount span')

        Lungo.Router.section("NewMovement");
    },
    'load section#NewMovement': function(){
        var el = $$('[data-control=carousel]')[0];
        carousel = Lungo.Element.Carousel(el, function(index, element) {
            Lungo.dom("[data-control='carousel'] > ul li.selected").removeClass();
            var selector = "[data-control='carousel'] > ul li:nth-child("+(index+2)+")";
            Lungo.dom(selector).addClass("selected");
        });
    },
    'load section#newBudget': function(){        
        var el = $$('[data-control=carouselBudget]')[0];
        carouselBudget = Lungo.Element.Carousel(el, function(index, element) {
            Lungo.dom("[data-control='carouselBudget'] > ul li.selected").removeClass();
            var selector = "[data-control='carouselBudget'] > ul li:nth-child("+(index+2)+")";
            Lungo.dom(selector).addClass("selected");
        });
    },    
    'tap section#NewMovement > article input#txt-date': function(){
        console.log("Tap txt-date");

        var inputFecha = Lungo.dom("section#NewMovement > article input#txt-date")[0].value;
        var separatorDate = "/";
        if(inputFecha === ""){ fechaCalendario = new Date(); }
        else{
            valueInputFecha = inputFecha.split(separatorDate);
            fechaCalendario = new Date(valueInputFecha[2], valueInputFecha[1]-1, valueInputFecha[0]);
        }
        Lungo.Notification.confirm({
            icon: '',
            title: Helpers.getTranslate('select_date'),
            description: Helpers.getTemplateSelectDayMonthYear(fechaCalendario),
            accept: {
                icon: 'checkmark',
                label: Helpers.getTranslate('accept'),
                callback: function(){
                    var inputScreen = Lungo.dom("#screen-date")[0];
                    date = Calendar.getActualDate();

                    dd = date.getDate();
                    mm = date.getMonth() + 1;
                    yyyy = date.getFullYear();

                    dd = ("0" + dd).slice(-2);
                    mm = ("0" + mm).slice(-2);


                    date = dd + '/' + mm + '/' + yyyy;

                    Lungo.dom("section#NewMovement > article input#txt-date")[0].value = date;
                }
            },
            cancel: {
                icon: 'close',
                label: Helpers.getTranslate('cancel'),
                callback: null
            }
        });

    },
    'tap section#NewMovement > article a[data-direction="left"]': function(){
        carousel.prev();
    },
    'tap section#NewMovement > article a[data-direction="right"]': function(){
        carousel.next();
    },
    'tap section#NewMovement > article div#carousel a.button': function(){
        console.log("button tag");
        Lungo.dom("#txt-tag").val("");
        Lungo.dom("#txt-tag").val(this.innerHTML);
    },
    'tap section#NewMovement > article div.category': function(){
        categoryClass = Lungo.dom("img#category")[0].className.split(" ");
        if(categoryClass[2] !== "add-money")
        {
            Lungo.dom("#category_control_back").val('section#NewMovement > article div.category img')
            Lungo.Router.section("newCategory");
        }

    },
    'tap section#newCategory > article li': function(event){
        var control_back = Lungo.dom("#category_control_back").val();
        Lungo.dom(control_back).removeClass();
        Lungo.dom(control_back).addClass("icon whallet "+ this.id);
        /*Lungo.dom("section#NewMovement > article div.category img").removeClass();
        Lungo.dom("section#NewMovement > article div.category img").addClass("icon whallet "+ this.id);*/
        categoryName = this.id;
        if(this.id === "tag-2"){
            categoryName = "wtag";
        }
        /*Lungo.dom("img#category")[0].src = "assets/images/categories/"+categoryName+".png"*/
        Lungo.dom(control_back)[0].src = "assets/images/categories/"+categoryName+".png"

        setTimeout((function() {
            return Lungo.Router.back();
        }), 500);
    },
    'tap section#NewMovement > article div.amount': function(){
        console.log("tap newAmount");
        user = Lungo.Data.Storage.persistent("userInfo");
        Lungo.dom("#screen")[0].textContent = "0"+ user.currency.decimal_mark +"00";
        Lungo.dom("#decimal_mark")[0].textContent = user.currency.decimal_mark;
        Lungo.Router.section("newAmount");
    },
    'tap section#newAmount > article#Form-Amount form div span' : function (event){
        user = Lungo.Data.Storage.persistent("userInfo");
        var inputScreen = Lungo.dom("#screen")[0];
        var valueScreen = inputScreen.textContent;
        var valuesScreen = valueScreen.split(user.currency.decimal_mark);
        var integerPart = valuesScreen[0];
        //var re = new RegExp("/"+user.currency.thousands_separator+"/g");
        //integerPart = integerPart.replace(re, "");
        while (integerPart.toString().indexOf(user.currency.thousands_separator) != -1)
            integerPart = integerPart.toString().replace(user.currency.thousands_separator,"");
        
        var decimalPart = valuesScreen[1];    

        var valueBtnPussed = this.textContent.replace(/^\s+|\s+$/g, '');

        if(event.srcElement.id == 'delete'){
            console.log("delete");
            if(decimalAdded)
            {
                if(countDecimal == 1)
                {
                    decimalPart = '00';
                    decimalAdded = false;
                    countDecimal = 0;
                }
                else
                {
                    decimalPart = decimalPart.substring(0,decimalPart.length-1) + '0'
                    countDecimal = 1;
                }
            }
            else
            {
                integerPart = integerPart.substring(0, integerPart.length-1);
                if(integerPart == '')
                {
                    integerPart = '0';
                }
            }
        }
        else if(valueBtnPussed == user.currency.decimal_mark){
            if(!decimalAdded){
                decimalAdded = true;
            }
        }
        else{
            if(countDecimal < 2)
            {
                if(!decimalAdded)
                {
                    if(integerPart == '0')
                    {
                        integerPart = valueBtnPussed;
                    }
                    else
                    {
                        integerPart += valueBtnPussed;
                    }
                }
                else
                {
                    countDecimal = countDecimal + 1;
                    if(countDecimal == 1)
                    {
                        decimalPart = valueBtnPussed + '0';
                    }
                    else if(countDecimal == 2)
                    {
                        decimalPart = decimalPart.substring(0,1) + valueBtnPussed;
                    }

                }
            }
        }


        integerPart = integerPart.toString().replace(/\B(?=(\d{3})+(?!\d))/g, user.currency.thousands_separator);

        inputScreen.innerHTML = integerPart + user.currency.decimal_mark + decimalPart;
    },
    'tap section#newAmount > article a#accept' : function (event){
        var inputScreen = Lungo.dom("#screen")[0];
        user = Lungo.Data.Storage.persistent("userInfo");

        control_back = Lungo.dom("#control_back").val();
        console.log(control_back);
        /*Lungo.dom("section#NewMovement > article div.amount span")[0].innerHTML = Helpers.userCurrencySymbol() + " " + inputScreen.textContent;*/
        Lungo.dom(control_back)[0].innerHTML = Helpers.userCurrencySymbol() + " " + inputScreen.textContent;        
        Lungo.dom("#screen")[0].textContent = "0"+ user.currency.decimal_mark +"00";
        countDecimal = 0;
        decimalAdded = false;
        Lungo.Router.back();
    }
});

document.onkeypress=function(e){
    var esIE = (document.all);
    var esNS = (document.layers);
    tecla = (esIE) ? event.keyCode : e.which;

    if(tecla == 13){
        return false;
      }
};
