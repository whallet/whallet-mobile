var Calendar = (function(lng, undefined){

    getActualDate = function(){
        var inputDay = Lungo.dom("div#dayCalendarMobile")[0].innerText;
        var inputMonth = Lungo.dom("div#monthCalendarMobile")[0].innerText;
        var inputYear = Lungo.dom("div#yearCalendarMobile")[0].innerText;
        var short_m_names = Helpers.getMonthShortNames();

        var monthNumber = short_m_names.indexOf(inputMonth.toUpperCase());

        return new Date(inputYear, monthNumber, inputDay, 0, 0, 0, 0);        
    };

    setDateScreen = function(date){
        yearCalendarMobile  = date.getFullYear();
        monthCalendarMobile = date.getMonth();
        dayCalendarMobile   = date.getDate();
        var m_names = Helpers.getMonthNames();

        //Variables String del calendario
        textMonthCalendarMobile = m_names[monthCalendarMobile].slice(0,3);
        textDayCalendarMobile   = ("0"+dayCalendarMobile).slice(-2);

        Lungo.dom("div#dayCalendarMobile")[0].innerText = textDayCalendarMobile;
        Lungo.dom("div#monthCalendarMobile")[0].innerText = textMonthCalendarMobile;
        Lungo.dom("div#yearCalendarMobile")[0].innerText = yearCalendarMobile;

        Lungo.dom("#screen-date")[0].innerHTML = Helpers.getElegantDate(date);
    };

    setShortDateScreen = function(date){
        yearCalendarMobile  = date.getFullYear();
        monthCalendarMobile = date.getMonth();
        dayCalendarMobile   = date.getDate();
        var m_names = Helpers.getMonthNames();

        //Variables String del calendario
        textMonthCalendarMobile = m_names[monthCalendarMobile].slice(0,3);
        textDayCalendarMobile   = ("0"+dayCalendarMobile).slice(-2);

        Lungo.dom("div#dayCalendarMobile")[0].innerText = textDayCalendarMobile;
        Lungo.dom("div#monthCalendarMobile")[0].innerText = textMonthCalendarMobile;
        Lungo.dom("div#yearCalendarMobile")[0].innerText = yearCalendarMobile;

        Lungo.dom("#screen-date")[0].innerHTML = Helpers.getFormatDate(date);  
    }

    addDay = function(){
        fechaCalendario = getActualDate();
        fechaCalendario = fechaCalendario.next().day();
        return fechaCalendario;
    };

    removeDay = function(){
        fechaCalendario = getActualDate();
        fechaCalendario = fechaCalendario.last().day();
        return fechaCalendario;
    };

    addMonth = function(){
        fechaCalendario = getActualDate();
        fechaCalendario = fechaCalendario.next().month();
        return fechaCalendario;
    };

    removeMonth = function(){
        fechaCalendario = getActualDate();
        fechaCalendario = fechaCalendario.last().month();
        return fechaCalendario;
    };

    addYear = function(){
        fechaCalendario = getActualDate();
        fechaCalendario = fechaCalendario.next().year();
        return fechaCalendario;
    };

    removeYear = function(){
        fechaCalendario = getActualDate();
        fechaCalendario = fechaCalendario.last().year();
        return fechaCalendario;
    };

    return {
        addDay              : addDay,
        removeDay           : removeDay,
        addMonth            : addMonth,
        removeMonth         : removeMonth,
        addYear             : addYear,
        removeYear          : removeYear,
        getActualDate       : getActualDate,    
        setDateScreen       : setDateScreen,
        setShortDateScreen  : setShortDateScreen
    };

})(Lungo);