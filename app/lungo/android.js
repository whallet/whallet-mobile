var Android = (function(lng, undefined){


    hideAd = function(){
    	if(User.isPro())
    	{
    		AndroidFunction.hideAd();
    	}
    };
    
    startBuyProcess = function(token){
    	console.log("startBuyProcess");
        
    	AndroidFunction.startBuyProcess(token);
    }

    return {
        hideAd           	: hideAd,
        startBuyProcess		: startBuyProcess
    };

})(Lungo);