var User = (function(lng, undefined){


    getUserName = function(){
        userInfo = Lungo.Data.Storage.persistent("userInfo");
        if(userInfo != null && userInfo.name != null){
          return userInfo.name
        } else {
          return Helpers.getTranslate('change_name');
        }
    };

    getCurrency = function(){
        userInfo = Lungo.Data.Storage.persistent("userInfo");
        if(userInfo != null && userInfo.currency != null){
          return userInfo.currency.name + " (" + userInfo.currency.symbol+ ")";
        } else {
          return Helpers.getTranslate('change_data');
        }
    };

    getCurrencyActive = function(){
        userInfo = Lungo.Data.Storage.persistent("userInfo");
        if(userInfo != null && userInfo.currency != null){
          return userInfo.currency;
      }
    };

    getLocale = function(){
        userInfo = Lungo.Data.Storage.persistent("userInfo");
        return userInfo.locale;
    };

    getUserInfo = function(){
      userInfo = Lungo.Data.Storage.persistent("userInfo");
      return userInfo;
    }

    changeLocale = function(locale){
      String.locale = locale;
      userInfo = Lungo.Data.Storage.persistent("userInfo");      
      userInfo.locale = locale;
      Lungo.Data.Storage.persistent("userInfo", userInfo);

      translateFragment();
      Helpers.updateAllTranslateElement();
      
      tokenStored = Lungo.Data.Storage.persistent("tokenAuth");
      url = "http://www.whallet.com/api/v1/user/update.json";
      post_data = {
        token: tokenStored,
        locale: locale
      };
      Lungo.Service.Settings.error = errorResponse;
      result = Lungo.Service.post(url, post_data, userUpdateLocaleResponse);
      
      return result;
    }

    userUpdateLocaleResponse = function(response) {
      console.log("userUpdateLocaleResponse");
      Lungo.Data.Storage.persistent("userInfo", response);
      Lungo.Notification.success(Helpers.getTranslate("success"), Helpers.getTranslate("locale_changed"), "check", 1, null);

      return Lungo.Router.back();
    };     

    errorResponse = function(type, xhr) {
      console.log("errorResponse");
      console.log("xhr: %o", xhr);
      return Lungo.Notification.error(Helpers.getTranslate("error"), Helpers.getTranslate("error_msg"), "cancel", 1, null);
    };    

    isPro = function(){
        userInfo = Lungo.Data.Storage.persistent("userInfo");
        if(userInfo != null && userInfo.pro != null){
          return userInfo.pro
        } else {
          return false;
        }
    };

    return {
        getUserInfo           : getUserInfo,
        getUserName          : getUserName,
        getCurrency          : getCurrency,
        getCurrencyActive       : getCurrencyActive,
        getLocale: getLocale,
        changeLocale : changeLocale,
        isPro        : isPro

    };

})(Lungo);